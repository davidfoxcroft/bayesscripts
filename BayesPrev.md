






Accuracy of Alcohol Screening Tests
-----------------------------------


### Background

To effectively screen people, practitioners need to know which test instrument is best. It needs to be quick, easy to administer, and identify problem drinkers with a high degree of accuracy (high sensitivity and specificity). Effective strategies are available to reduce excessive drinking, ranging from identification and brief advice to more specialised services to treat dependent drinkers. Brief interventions in particular have been shown to be effective and cost-effective for reducing alcohol consumption in people identified through opportunistic screening in primary care settings as hazardous or harmful drinkers [@Kaner2007; @Kaner2009; @Ludbrook2004; @Ludbrook2001]. 

The accuracy of screening tests for identifying problem drinkers in primary care has recently been evaluated in an earlier ARUK funded International Cochrane Collaboration systematic review and meta-analysis [ref]. We did this by analysing studies that compared the performance of a test with a structured interview as a reference standard. We found that AUDIT had good accuracy for identifying alcohol abuse or dependence and, in a separate analysis, that AUDIT-C had good accuracy for identification of hazardous drinking. Importantly, AUDIT identified a greater proportion of true positives and true negatives for hazardous drinking with a threshold score of 5 rather than the recommended 8, for both men and women. A study to verify this finding is warranted, particularly since accuracy studies in UK primary care settings, particularly with younger adult drinkers who are more likely to drink to excess, are lacking. Our Cochrane review did not find any appropriate studies for a UK primary care population that focused on young adults aged 18-35 years or that included women. 

In the current accuracy study we aimed to validate brief alcohol screening tests with young adults in a UK primary care population for the detection of hazardous drinking and alcohol disorders. In particular, the accuracy of screening tests for identifying hazardous drinkers, rather than people with alcohol dependence, is not well established. Individual studies have produced variable results. For example, estimates for AUDIT score 8 varied from 0.31 to 0.89 for sensitivity, and 0.83 to 0.96 for specificity [@OConnell2004]. The diagnostic performance of these tests may vary according to the definition used for hazardous or harmful drinking, and other factors such as age [@Berner2007; @Reinert2002], sex [@Bradley1998; @Reinert2002] and ethnicity [@Reinert2002]. 

We also aimed to develop statistical models, using Bayes Theorem, to predict prevalence rates of hazardous drinking and alcohol disorders for discrete screening test scores. In another earlier study [@Foxcroft2009] we demonstrated a technique using Bayes Theorem to model the population prevalence (post-test probability) of hazardous drinking or alcohol use disorders based on discrete test scores when combined with baseline (pre-test) prevalence levels. Using this model, we plotted a curve showing the post-test probability of alcohol dependence for all possible AUDIT scores (0-40), using data from the US and New Zealand, showing that the prevalence of a condition, indicated by post-test probability estimates using Bayes Theorem, varies according to the slope of a curve across the range of test scores.

### Method

#### Design: 
Cross-sectional study

#### Setting: 
Fourteen [is this the right final number?] primary care practices in the Thames Valley, selected to give a diverse mix of socio-economic backgrounds and rural/urban locations to improve the generalisability of the study results.

#### Participants, recruitment and procedures: 
A random selection of xxxxx [final number?] adults aged 18 to 35 years registered as patients at selected practices. These patients were sent by post information about the study along with a General Lifestyle Questionnaire (GLQ). The GLQ contained questions addressing alcohol, diet, exercise, and smoking taken from the General Lifestyle Survey [@ONS2010]. The alcohol screening instrument questions (AUDIT and AUDIT-C) were embedded within the GLQ to reduce sensitivity of the subject. Participants had a choice of not completing the questionnaire or returning the completed questionnaire to the researchers in a pre-paid envelope with or without providing full contact details. Non-English speakers, adults with special educational needs or undergoing current substance misuse treatment including alcohol were excluded.

Participants who provided contact details were invited to take part in the in-depth interview phase of the study. Computer-assisted telephone interviews were conducted with consenting participants within 7-14 [did we achieve this?] days by trained researchers who were blind to the patients’ screening test scores. To encourage responsiveness, individuals were offered a cash incentive [@Edwards2009] for completing and returning the questionnaire and being selected for interview. A reminder postcard after two weeks was sent to individuals who had not returned the questionnaire [@Edwards2009] [did we do this all the time]. 


#### Data collection: 

1. Index tests
The AUDIT is a 10 item questionnaire consisting of questions on quantity and frequency of alcohol consumption, alcohol-related problems and dependency symptoms [@Babor2001]. Each item is scored from 0-4, and items are summed to generate a total score from 0 to 40. AUDIT-C comprises the first 3 questions on AUDIT asking about quantity and frequency of consumption and has been used as a short independent version of AUDIT [@Bush1998]. AUDIT-C is scored on a scale of 0-12. AUDIT questions were embedded in the GLQ, taking on average 15 minutes to complete. Participants were able to complete a paper version of the questionnaire and return it via a pre-paid envelope or, alternatively, they could complete the questionnaire online. Previous research has shown that self-administered paper or computerised questionnaires give similar results [@Barry1990; @Dillman2000].

2. Reference tests
In-depth interviews conducted by a trained researcher used: 

(a) the Timeline Followback (TLFB) procedure to ascertain quantity and frequency of alcohol consumption in the previous 90 days. Research has shown this is a valid and reliable method for deriving quantity/frequency of alcohol consumption in clinical and non-clinical populations [@Sobell1988; @Sobell1992]. Quantity of alcohol was standardized into units (UK). The TLFB interview section took 20-30 minutes to complete. This method was used to establish how frequently the patient had exceed recommended drinking levels (more than 14/21 units of alcohol in any one week or 2/3 units a day for five consecutive days) and was used as the main criterion for hazardous drinking. TLFB data was also be used to derive the frequency of binge consumption (more than 6/8 units on a single episode). 

(b) the alcohol dependence section of the short form computerized Composite International Diagnostic Interview (CIDI); a valid and reliable method for the diagnosis of alcohol dependence according to Diagnostic and Statistical Manual Disorders (DSM IV) criteria [@Forman2004]. The computerised CIDI is designed to be administered by clinical and non-clinical interviewers; it comprises seven questions and took 10-20 minutes to administer. 


#### Sample size: 
Our primary aim was to compare AUDIT-C and AUDIT with the appropriate reference tests for identification of hazardous drinking and alcohol abuse or dependence. The accuracy of screening instruments may vary according to gender so we planned to examine differences in two subgroups: men aged 18-35 and women aged 18-35. We required 259 women and 139 men (N=398) for validation of hazardous drinking, assuming 80% sensitivity (based on meta-analysis results), confidence intervals of +/- 10% (based on precedent from [@Coulton2006]), prevalence levels from UK Adult Psychiatric Morbidity Survey 2007 and conventional 5% alpha. We therefore aimed to recruit 450 participants to allow for any problems with missing data. For validation of alcohol abuse or dependence we would have needed 414 men and 1,007 women. This was not achievable within the level of resource available, and therefore the validation of alcohol abuse or dependence tests in our analyses has less precision. 


#### Analysis: 
Linear relations between quantity of alcohol consumed (units/week) and test scores was examined by correlation. Regression was used to examine relations between socio-demographic factors, test scores and prevalence of hazardous drinking (above recommended limits: 3/4 units per day or 21 units per week men; 2/3 units per day or 14 units per week for women), binge drinking (>8 units men or > 6 units women on a single occasion), and alcohol abuse or dependence; according to TLFB and DSM criteria.

Accuracy of the screening tests to identify hazardous drinking or alcohol abuse or dependence was assessed by constructing receiver operator characteristic (ROC) curves for all possible values of test results. Sensitivity, specificity, positive and negative predictive values were calculated, along with positive likelihood ratios with 95% confidence intervals. An unweighted and two weighted Youden J Index scores were also calculated to indicate potential optimal threshold (cut-point) test scores. 

We calculated post-test probability functions (curves) by reference test, index test and gender. Standard errors were estimated using a bootstrap technique to generate many sets of simulated data to which the fit function was applied. These standard errors, along with the pre-test probability standard error, were propagated through the Bayes Theorem calculation to provide a standard error estimate for each test score post-test probability [@Pezzullo2007].

Analysis was undertaken using the [R] statistical programming language [@RDevelopmentCoreTeam2008; @pROC2011; @Bates2013] and [SPSS...STATA...Lesley?]. To assess the clustered nature of the data, as patients were recruited by General Practice, we used hierarchical binary logistic regressions (procedure glmer in [R] [@Bates2013]) to calculate parameter estimates and standard errors. These were then converted to Odds Ratios and 95% confidence intervals. Diagnostic / Screening accuracy statistics were calculated using the EpiR and the pROC procedures in [R]. Bayes theorem calculations were undertaken using pre-programmed [R] routines [@Foxcroft2009] and checked against results from the simplebayes procedure in [R].


### Results
[quick summary tables / fig. below, but need to discuss what else should go in here...

suggest: response rates, sample characteristics on other variables (? what) and compared with data available from other studies, e.g. national sample surveys]




<br><h4>Descriptives and Summary across different Tests and Reference Outcomes:</h4><p><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Sample characteristics: summary by Gender</b> </CAPTION>
<TR> <TH>   </TH> <TH> Males, N=138 </TH> <TH>   </TH> <TH> Females, N=282 </TH> <TH>   </TH>  </TR>
  <TR> <TD>  </TD> <TD> % </TD> <TD> N </TD> <TD> % </TD> <TD> N </TD> </TR>
  <TR> <TD> Hazardous Drinking (any) </TD> <TD> 48.6 </TD> <TD> 67 </TD> <TD> 51.1 </TD> <TD> 144 </TD> </TR>
  <TR> <TD> Binge drinker (any) </TD> <TD> 71.0 </TD> <TD> 98 </TD> <TD> 65.6 </TD> <TD> 185 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Dependence </TD> <TD> 13.0 </TD> <TD> 18 </TD> <TD>  8.5 </TD> <TD> 24 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Abuse </TD> <TD> 35.5 </TD> <TD> 49 </TD> <TD> 18.8 </TD> <TD> 53 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none vs. mild-severe </TD> <TD> 52.2 </TD> <TD> 72 </TD> <TD> 39.7 </TD> <TD> 112 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe </TD> <TD> 22.5 </TD> <TD> 31 </TD> <TD> 11.0 </TD> <TD> 31 </TD> </TR>
  <TR> <TD>  </TD> <TD> mean </TD> <TD> s.e. </TD> <TD> mean </TD> <TD> s.e. </TD> </TR>
  <TR> <TD> IMD score </TD> <TD> 10.76 </TD> <TD> 0.69 </TD> <TD> 10.51 </TD> <TD> 0.49 </TD> </TR>
  <TR> <TD> AUDIT score </TD> <TD>  8.08 </TD> <TD> 0.50 </TD> <TD>  5.44 </TD> <TD> 0.26 </TD> </TR>
  <TR> <TD> AUDITC score </TD> <TD>  5.02 </TD> <TD> 0.23 </TD> <TD>  3.95 </TD> <TD> 0.15 </TD> </TR>
   </TABLE>
<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Study sample breakdown by Lower Layer Super Output Area IMD Quintiles for England (2010)</b> </CAPTION>
<TR> <TH> Quintile, high=higher deprivation </TH> <TH> N </TH> <TH> % </TH>  </TR>
  <TR> <TD> 1 </TD> <TD> 211 </TD> <TD> 53.15 </TD> </TR>
  <TR> <TD> 2 </TD> <TD>  73 </TD> <TD> 18.39 </TD> </TR>
  <TR> <TD> 3 </TD> <TD>  72 </TD> <TD> 18.14 </TD> </TR>
  <TR> <TD> 4 </TD> <TD>  36 </TD> <TD>  9.07 </TD> </TR>
  <TR> <TD> 5 </TD> <TD>   5 </TD> <TD>  1.26 </TD> </TR>
   </TABLE>
<br><br><br>![plot of chunk showsum](figure/showsum1.png) <br><center><b>Figure: Boxplots of median, min and max IMD score by Study Practice; with LLSOA Quintles for England</center></b><br><br><br><br><br><!-- Map generated in R 3.0.2 by googleVis 0.4.7 package -->
<!-- Thu Jul 31 17:31:15 2014 -->


<!-- jsHeader -->
<script type="text/javascript">
 
// jsData 
function gvisDataMapID21334c08d4e0 () {
var data = new google.visualization.DataTable();
var datajson =
[
 [
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8229325,
-0.8313162,
"51.8229325:-0.8313162" 
],
[
 51.8205216,
-0.7980853,
"51.8205216:-0.7980853" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8229325,
-0.8313162,
"51.8229325:-0.8313162" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8019832,
-0.8065892,
"51.8019832:-0.8065892" 
],
[
 51.8205216,
-0.7980853,
"51.8205216:-0.7980853" 
],
[
 51.8391447,
-0.7775157,
"51.8391447:-0.7775157" 
],
[
 51.9194877,
-1.2657848,
"51.9194877:-1.2657848" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9194877,
-1.2657848,
"51.9194877:-1.2657848" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9521009,
-1.1817751,
"51.9521009:-1.1817751" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9194877,
-1.2657848,
"51.9194877:-1.2657848" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.4822815,
-0.5301374,
"51.4822815:-0.5301374" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9521009,
-1.1817751,
"51.9521009:-1.1817751" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9194877,
-1.2657848,
"51.9194877:-1.2657848" 
],
[
 51.6555466,
-1.2659382,
"51.6555466:-1.2659382" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.446708,
0.0175553,
"51.4467080:0.0175553" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.6862059,
-1.3521783,
"51.6862059:-1.3521783" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.6555466,
-1.2659382,
"51.6555466:-1.2659382" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.7710968,
-1.3076558,
"51.7710968:-1.3076558" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.5713463,
-1.2621212,
"51.5713463:-1.2621212" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9928259,
-0.7272976,
"51.9928259:-0.7272976" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9877859,
-0.6848056,
"51.9877859:-0.6848056" 
],
[
 51.9928259,
-0.7272976,
"51.9928259:-0.7272976" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9928259,
-0.7272976,
"51.9928259:-0.7272976" 
],
[
 51.9928259,
-0.7272976,
"51.9928259:-0.7272976" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9877859,
-0.6848056,
"51.9877859:-0.6848056" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9877859,
-0.6848056,
"51.9877859:-0.6848056" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 52.0003978,
-0.7850682,
"52.0003978:-0.7850682" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.9926742,
-0.7515198,
"51.9926742:-0.7515198" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7803809,
-1.4176563,
"51.7803809:-1.4176563" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7803809,
-1.4176563,
"51.7803809:-1.4176563" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7363459,
-1.5616434,
"51.7363459:-1.5616434" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7363459,
-1.5616434,
"51.7363459:-1.5616434" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7803809,
-1.4176563,
"51.7803809:-1.4176563" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7803809,
-1.4176563,
"51.7803809:-1.4176563" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7848307,
-1.4849675,
"51.7848307:-1.4849675" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7721343,
-1.2105434,
"51.7721343:-1.2105434" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7239721,
-1.2751145,
"51.7239721:-1.2751145" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.7246123,
-1.2145364,
"51.7246123:-1.2145364" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.5337514,
-0.7332794,
"51.5337514:-0.7332794" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.3966564,
-0.8127657,
"51.3966564:-0.8127657" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.3966564,
-0.8127657,
"51.3966564:-0.8127657" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.3966564,
-0.8127657,
"51.3966564:-0.8127657" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.3966564,
-0.8127657,
"51.3966564:-0.8127657" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 51.4120147,
-0.8667241,
"51.4120147:-0.8667241" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 52.0668556,
-1.3462151,
"52.0668556:-1.3462151" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5697031,
-1.4069794,
"51.5697031:-1.4069794" 
],
[
 51.5337514,
-0.7332794,
"51.5337514:-0.7332794" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.5337514,
-0.7332794,
"51.5337514:-0.7332794" 
],
[
 51.3150762,
-0.3389428,
"51.3150762:-0.3389428" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.5450105,
-0.6026154,
"51.5450105:-0.6026154" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.539466,
-0.6560172,
"51.5394660:-0.6560172" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 52.1199935,
-1.3364792,
"52.1199935:-1.3364792" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9521009,
-1.1817751,
"51.9521009:-1.1817751" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.9048141,
-1.1493204,
"51.9048141:-1.1493204" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.4678568,
-2.600082,
"51.4678568:-2.6000820" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
],
[
 51.7751249,
-0.8720555,
"51.7751249:-0.8720555" 
] 
];
data.addColumn('number','Latitude');
data.addColumn('number','Longitude');
data.addColumn('string','latlong.lat');
data.addRows(datajson);
return(data);
}
 
// jsDrawChart
function drawChartMapID21334c08d4e0() {
var data = gvisDataMapID21334c08d4e0();
var options = {};
options["showTip"] = true;
options["mapType"] = "normal";

    var chart = new google.visualization.Map(
    document.getElementById('MapID21334c08d4e0')
    );
    chart.draw(data,options);
    

}
  
 
// jsDisplayChart
(function() {
var pkgs = window.__gvisPackages = window.__gvisPackages || [];
var callbacks = window.__gvisCallbacks = window.__gvisCallbacks || [];
var chartid = "map";
  
// Manually see if chartid is in pkgs (not all browsers support Array.indexOf)
var i, newPackage = true;
for (i = 0; newPackage && i < pkgs.length; i++) {
if (pkgs[i] === chartid)
newPackage = false;
}
if (newPackage)
  pkgs.push(chartid);
  
// Add the drawChart function to the global list of callbacks
callbacks.push(drawChartMapID21334c08d4e0);
})();
function displayChartMapID21334c08d4e0() {
  var pkgs = window.__gvisPackages = window.__gvisPackages || [];
  var callbacks = window.__gvisCallbacks = window.__gvisCallbacks || [];
  window.clearTimeout(window.__gvisLoad);
  // The timeout is set to 100 because otherwise the container div we are
  // targeting might not be part of the document yet
  window.__gvisLoad = setTimeout(function() {
  var pkgCount = pkgs.length;
  google.load("visualization", "1", { packages:pkgs, callback: function() {
  if (pkgCount != pkgs.length) {
  // Race condition where another setTimeout call snuck in after us; if
  // that call added a package, we must not shift its callback
  return;
}
while (callbacks.length > 0)
callbacks.shift()();
} });
}, 100);
}
 
// jsFooter
</script>
 
<!-- jsChart -->  
<script type="text/javascript" src="https://www.google.com/jsapi?callback=displayChartMapID21334c08d4e0"></script>
 
<!-- divChart -->
  
<div id="MapID21334c08d4e0"
  style="width: 600px; height: 500px;">
</div>
<br><center><b>Figure: Google Map Plot of Sample Geography, based on postcode sectors</center></b><br><br><br><br><br>![plot of chunk showsum](figure/showsum2.png) <br><center><b>Figure: Pearson correlations between Reference Outcomes and Total 90 day consumption</center></b><br><br><br><br><br>![plot of chunk showsum](figure/showsum3.png) <br><center><b>Figure: Pearson correlations between Reference Outcomes and Number of Heavy Drinking Days (out of 90 days)</center></b><br><br><br><br><br><br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: ICC by Practice (N=14) and GLM Odds Ratos for Reference Outcomes by Index of Multiple Deprivation score, Gender, Age Group and Smoking Status</b> </CAPTION>
<TR> <TH>   </TH> <TH> ICC by Practice </TH> <TH> Odds Ratio, IMD </TH> <TH> Odds Ratio, Gender </TH> <TH> Odds Ratio, Age Group2 </TH> <TH> Odds Ratio, Age Group3 </TH> <TH> Odds Ratio, CurrentSmoker2 </TH> <TH> Odds Ratio, CurrentSmoker3 </TH>  </TR>
  <TR> <TD> Hazardous Drinking (any) </TD> <TD> 0.01 (95% CI -0.01 to 0.08) </TD> <TD> 0.99 (95% CI  0.97  to  1.02 ) </TD> <TD> 1.39 (95% CI  0.89  to  2.18 ) </TD> <TD> 0.90 (95% CI  0.52  to  1.56 ) </TD> <TD> 1.09 (95% CI  0.65  to  1.82 ) </TD> <TD> 0.67 (95% CI  0.35  to  1.28 ) </TD> <TD> 0.28 (95% CI  0.15  to  0.50 ) </TD> </TR>
  <TR> <TD> Binge drinker (any) </TD> <TD> 0.03 (95% CI -0.00 to 0.11) </TD> <TD>  0.99 (95% CI   0.96  to   1.02 ) </TD> <TD>  0.84 (95% CI   0.52  to   1.35 ) </TD> <TD>  0.69 (95% CI   0.38  to   1.24 ) </TD> <TD>  0.67 (95% CI   0.38  to   1.17 ) </TD> <TD>  0.81 (95% CI   0.38  to   1.65 ) </TD> <TD>  0.47 (95% CI   0.24  to   0.89 ) </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Dependence </TD> <TD> 0.04 (95% CI  0.00 to 0.13) </TD> <TD> 1.02 (95% CI  0.98  to  1.06 ) </TD> <TD> 0.51 (95% CI  0.25  to  1.02 ) </TD> <TD> 1.22 (95% CI  0.52  to  2.92 ) </TD> <TD> 0.74 (95% CI  0.31  to  1.80 ) </TD> <TD> 0.35 (95% CI  0.14  to  0.84 ) </TD> <TD> 0.29 (95% CI  0.13  to  0.66 ) </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Abuse </TD> <TD> 0.03 (95% CI  0.00 to 0.12) </TD> <TD> 0.97 (95% CI  0.93  to  1.00 ) </TD> <TD> 0.48 (95% CI  0.29  to  0.80 ) </TD> <TD> 1.08 (95% CI  0.56  to  2.11 ) </TD> <TD> 1.15 (95% CI  0.63  to  2.15 ) </TD> <TD> 0.94 (95% CI  0.49  to  1.82 ) </TD> <TD> 0.26 (95% CI  0.13  to  0.51 ) </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none vs. mild-severe </TD> <TD> 0.02 (95% CI -0.01 to 0.10) </TD> <TD> 0.99 (95% CI  0.96  to  1.01 ) </TD> <TD> 0.66 (95% CI  0.42  to  1.04 ) </TD> <TD> 1.16 (95% CI  0.67  to  2.03 ) </TD> <TD> 0.93 (95% CI  0.55  to  1.58 ) </TD> <TD> 0.94 (95% CI  0.50  to  1.76 ) </TD> <TD> 0.30 (95% CI  0.17  to  0.54 ) </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe </TD> <TD> 0.02 (95% CI -0.00 to 0.11) </TD> <TD> 0.99 (95% CI  0.95  to  1.03 ) </TD> <TD> 0.40 (95% CI  0.22  to  0.73 ) </TD> <TD> 2.14 (95% CI  0.96  to  5.03 ) </TD> <TD> 1.63 (95% CI  0.75  to  3.71 ) </TD> <TD> 0.92 (95% CI  0.45  to  1.93 ) </TD> <TD> 0.21 (95% CI  0.09  to  0.46 ) </TD> </TR>
   </TABLE>
<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: ICC by Practice (N=14) and HLM Odds Ratos for Reference Outcomes by Index of Multiple Deprivation (IMD) score, Gender, Age Group and Smoking Status</b> </CAPTION>
<TR> <TH>   </TH> <TH> ICC by Practice </TH> <TH> Odds Ratio, IMD </TH> <TH> Odds Ratio, Gender </TH> <TH> Odds Ratio, Age Group2 </TH> <TH> Odds Ratio, Age Group3 </TH> <TH> Odds Ratio, CurrentSmoker2 </TH> <TH> Odds Ratio, CurrentSmoker3 </TH>  </TR>
  <TR> <TD> Hazardous Drinking (any) </TD> <TD> 0.01 </TD> <TD> 0.88 (95% CI  0.56  to  1.39 ) </TD> <TD> 1.39 (95% CI  0.92  to  2.17 ) </TD> <TD> 0.90 (95% CI  0.49  to  1.55 ) </TD> <TD> 1.09 (95% CI  0.64  to  1.83 ) </TD> <TD> 0.67 (95% CI  0.34  to  1.30 ) </TD> <TD> 0.28 (95% CI  0.13  to  0.48 ) </TD> </TR>
  <TR> <TD> Binge drinker (any) </TD> <TD> 0.02 </TD> <TD> 0.89 (95% CI  0.56  to  1.50 ) </TD> <TD> 0.84 (95% CI  0.51  to  1.35 ) </TD> <TD> 0.69 (95% CI  0.38  to  1.21 ) </TD> <TD> 0.67 (95% CI  0.37  to  1.17 ) </TD> <TD> 0.82 (95% CI  0.37  to  1.74 ) </TD> <TD> 0.48 (95% CI  0.23  to  0.93 ) </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Dependence </TD> <TD> 0.11 </TD> <TD> 1.68 (95% CI  0.70  to  3.31 ) </TD> <TD> 0.63 (95% CI  0.27  to  1.46 ) </TD> <TD> 1.19 (95% CI  0.48  to  3.12 ) </TD> <TD> 0.70 (95% CI  0.30  to  1.99 ) </TD> <TD> 0.32 (95% CI  0.12  to  0.83 ) </TD> <TD> 0.26 (95% CI  0.12  to  0.63 ) </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Abuse </TD> <TD> 0.07 </TD> <TD> 0.56 (95% CI  0.27  to  0.94 ) </TD> <TD> 0.49 (95% CI  0.25  to  0.85 ) </TD> <TD> 1.13 (95% CI  0.57  to  2.40 ) </TD> <TD> 1.19 (95% CI  0.61  to  2.36 ) </TD> <TD> 0.95 (95% CI  0.46  to  1.87 ) </TD> <TD> 0.26 (95% CI  0.13  to  0.52 ) </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none vs. mild-severe </TD> <TD> 0.04 </TD> <TD> 0.83 (95% CI  0.55  to  1.32 ) </TD> <TD> 0.68 (95% CI  0.41  to  1.10 ) </TD> <TD> 1.18 (95% CI  0.68  to  2.07 ) </TD> <TD> 0.93 (95% CI  0.56  to  1.71 ) </TD> <TD> 0.94 (95% CI  0.47  to  1.76 ) </TD> <TD> 0.30 (95% CI  0.16  to  0.54 ) </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe </TD> <TD> 0.07 </TD> <TD> 0.91 (95% CI  0.44  to  1.64 ) </TD> <TD> 0.44 (95% CI  0.21  to  0.86 ) </TD> <TD> 2.18 (95% CI  1.00  to  5.70 ) </TD> <TD> 1.58 (95% CI  0.74  to  4.06 ) </TD> <TD> 0.91 (95% CI  0.42  to  2.03 ) </TD> <TD> 0.20 (95% CI  0.08  to  0.50 ) </TD> </TR>
   </TABLE>
<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: ICC by Practice (N=14) and GLMM beta coefficients for other TLFB Outcomes by Index of Multiple Deprivation (IMD) score, Gender, Age Group and Smoking Status</b> </CAPTION>
<TR> <TH>  </TH> <TH> ICC </TH> <TH> IMD, beta </TH> <TH> Gender, beta </TH> <TH> Agegrp2, beta </TH> <TH> Agegrp3, beta </TH> <TH> CurrentSmoker2, beta </TH> <TH> Currentsmoker3, beta </TH> <TH> AUDIT-C score, beta </TH>  </TR>
  <TR> <TD> Total Consumption (Units) over Last 90 </TD> <TD> 0.00 </TD> <TD> -0.06 (95% CI  -0.24  to   0.13 ) </TD> <TD> -0.29 (95% CI  -0.49  to  -0.09 ) </TD> <TD> -0.06 (95% CI  -0.31  to   0.18 ) </TD> <TD>  0.19 (95% CI  -0.04  to   0.42 ) </TD> <TD>  0.31 (95% CI   0.03  to   0.60 ) </TD> <TD>  0.06 (95% CI  -0.21  to   0.33 ) </TD> <TD>  2.16 (95% CI   1.97  to   2.35 ) </TD> </TR>
  <TR> <TD> Number of Heavy Drinking Days in Last 90 </TD> <TD> 0.00 </TD> <TD>  0.00 (95% CI  -0.17  to   0.18 ) </TD> <TD>  0.06 (95% CI  -0.13  to   0.25 ) </TD> <TD>  0.00 (95% CI  -0.22  to   0.23 ) </TD> <TD>  0.23 (95% CI   0.01  to   0.44 ) </TD> <TD>  0.31 (95% CI   0.04  to   0.59 ) </TD> <TD>  0.07 (95% CI  -0.18  to   0.32 ) </TD> <TD>  1.80 (95% CI   1.61  to   1.98 ) </TD> </TR>
   </TABLE>
<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <b>Table: AUDIT test score thresholds (cut-points) for different reference outcomes<br><br></b> </CAPTION>
<TR> <TH> Reference Outcome, by Test Index </TH> <TH> Youden J threshold </TH> <TH> Jw; weighted for Sensitivity </TH> <TH> Jw; weighted for Specificity </TH>  </TR>
  <TR> <TD> Hazardous Drinking (any): Males </TD> <TD> 9 </TD> <TD> 5 </TD> <TD> 11 </TD> </TR>
  <TR> <TD> Hazardous Drinking (any): Females </TD> <TD> 4 </TD> <TD> 2 </TD> <TD> 7 </TD> </TR>
  <TR> <TD> Binge drinker (any): Males </TD> <TD> 7 </TD> <TD> 3 </TD> <TD> 7 </TD> </TR>
  <TR> <TD> Binge drinker (any): Females </TD> <TD> 4 </TD> <TD> 2 </TD> <TD> 7 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Dependence: Males </TD> <TD> 12 </TD> <TD> 9 </TD> <TD> 12 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Dependence: Females </TD> <TD> 7 </TD> <TD> 2 </TD> <TD> 11 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Abuse: Males </TD> <TD> 10 </TD> <TD> 5 </TD> <TD> 15 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Abuse: Females </TD> <TD> 5 </TD> <TD> 2 </TD> <TD> 10 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none vs. mild-severe: Males </TD> <TD> 9 </TD> <TD> 4 </TD> <TD> 13 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none vs. mild-severe: Females </TD> <TD> 6 </TD> <TD> 2 </TD> <TD> 11 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe: Males </TD> <TD> 9 </TD> <TD> 4 </TD> <TD> 13 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe: Females </TD> <TD> 8 </TD> <TD> 3 </TD> <TD> 12 </TD> </TR>
   </TABLE>
<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <b>Table: AUDIT-C test score thresholds (cut-points) for different reference outcomes<br><br></b> </CAPTION>
<TR> <TH> Reference Outcome, by Test Index </TH> <TH> Youden J threshold </TH> <TH> Jw; weighted for Sensitivity </TH> <TH> Jw; weighted for Specificity </TH>  </TR>
  <TR> <TD> Hazardous Drinking (any): Males </TD> <TD> 5 </TD> <TD> 4 </TD> <TD> 7 </TD> </TR>
  <TR> <TD> Hazardous Drinking (any): Females </TD> <TD> 4 </TD> <TD> 2 </TD> <TD> 6 </TD> </TR>
  <TR> <TD> Binge drinker (any): Males </TD> <TD> 5 </TD> <TD> 4 </TD> <TD> 6 </TD> </TR>
  <TR> <TD> Binge drinker (any): Females </TD> <TD> 3 </TD> <TD> 2 </TD> <TD> 5 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Dependence: Males </TD> <TD> 6 </TD> <TD> 3 </TD> <TD> 11 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Dependence: Females </TD> <TD> 7 </TD> <TD> 2 </TD> <TD> 7 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Abuse: Males </TD> <TD> 4 </TD> <TD> 2 </TD> <TD> 9 </TD> </TR>
  <TR> <TD> DSM-IV Alcohol Abuse: Females </TD> <TD> 5 </TD> <TD> 2 </TD> <TD> 10 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none vs. mild-severe: Males </TD> <TD> 6 </TD> <TD> 2 </TD> <TD> 9 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none vs. mild-severe: Females </TD> <TD> 4 </TD> <TD> 2 </TD> <TD> 9 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe: Males </TD> <TD> 6 </TD> <TD> 2 </TD> <TD> 11 </TD> </TR>
  <TR> <TD> DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe: Females </TD> <TD> 6 </TD> <TD> 2 </TD> <TD> 10 </TD> </TR>
   </TABLE>
<br><br><br>



### Discussion

[To be added, but key points from me are:

 - summarise sample, response rates and sample limitations
 - summarise test threshold findings
 - critical discussion of limitations of using test thresholds
 - recommend move to a bayesian perspective in SBI where post-test probabilities are recommended (and achievable with modern computer technology)
 - need for more data / research from other areas / age groups / IMD profiles
 - need for development of computer based technology for bayesian SBI approach
 - need for RCTs to test effectiveness of above
 
]

**********


## Detailed Analyses
[Need to consider which information to pull out for inclusion in main body of a report / paper...]


<h4><u>1. Hazardous Drinking (any) by AUDIT Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails1.png) <br><center><b>Figure: Predicting Hazardous Drinking (any) with AUDIT score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting Hazardous Drinking (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Hazardous Drinking (any) with AUDIT score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 4 </TD> <TD> 65 </TD> <TD> 47 </TD> <TD> 2 </TD> <TD> 24 </TD> <TD> 0.97 (0.90 to 1.00) </TD> <TD> 0.34 (0.23 to 0.46) </TD> <TD> 0.58 (0.48 to 0.67) </TD> <TD> 0.92 (0.75 to 0.99) </TD> <TD> 1.47 (1.23 to 1.74) </TD> <TD> 0.31 (0.13 to 0.46) </TD> <TD> 0.62 </TD> <TD> -0.01 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 62 </TD> <TD> 37 </TD> <TD> 5 </TD> <TD> 34 </TD> <TD> 0.93 (0.83 to 0.98) </TD> <TD> 0.48 (0.36 to 0.60) </TD> <TD> 0.63 (0.52 to 0.72) </TD> <TD> 0.87 (0.73 to 0.96) </TD> <TD> 1.78 (1.41 to 2.24) </TD> <TD> 0.40 (0.19 to 0.58) </TD> <TD> 0.64 </TD> <TD> 0.19 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 56 </TD> <TD> 30 </TD> <TD> 11 </TD> <TD> 41 </TD> <TD> 0.84 (0.73 to 0.92) </TD> <TD> 0.58 (0.45 to 0.69) </TD> <TD> 0.65 (0.54 to 0.75) </TD> <TD> 0.79 (0.65 to 0.89) </TD> <TD> 1.98 (1.48 to 2.65) </TD> <TD> 0.41 (0.18 to 0.61) </TD> <TD> 0.55 </TD> <TD> 0.29 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 50 </TD> <TD> 23 </TD> <TD> 17 </TD> <TD> 48 </TD> <TD> 0.75 (0.63 to 0.84) </TD> <TD> 0.68 (0.55 to 0.78) </TD> <TD> 0.68 (0.57 to 0.79) </TD> <TD> 0.74 (0.61 to 0.84) </TD> <TD> 2.30 (1.60 to 3.31) </TD> <TD> 0.42 (0.18 to 0.63) </TD> <TD> 0.47 </TD> <TD> 0.40 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 45 </TD> <TD> 19 </TD> <TD> 22 </TD> <TD> 52 </TD> <TD> 0.67 (0.55 to 0.78) </TD> <TD> 0.73 (0.61 to 0.83) </TD> <TD> 0.70 (0.58 to 0.81) </TD> <TD> 0.70 (0.59 to 0.80) </TD> <TD> 2.51 (1.65 to 3.82) </TD> <TD> 0.40 (0.16 to 0.61) </TD> <TD> 0.37 </TD> <TD> 0.43 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 43 </TD> <TD> 13 </TD> <TD> 24 </TD> <TD> 58 </TD> <TD> 0.64 (0.52 to 0.76) </TD> <TD> 0.82 (0.71 to 0.90) </TD> <TD> 0.77 (0.64 to 0.87) </TD> <TD> 0.71 (0.60 to 0.80) </TD> <TD> 3.51 (2.08 to 5.91) </TD> <TD> 0.46 (0.22 to 0.65) </TD> <TD> 0.37 </TD> <TD> 0.55 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 38 </TD> <TD> 9 </TD> <TD> 29 </TD> <TD> 62 </TD> <TD> 0.57 (0.44 to 0.69) </TD> <TD> 0.87 (0.77 to 0.94) </TD> <TD> 0.81 (0.67 to 0.91) </TD> <TD> 0.68 (0.58 to 0.78) </TD> <TD> 4.47 (2.35 to 8.53) </TD> <TD> 0.44 (0.21 to 0.63) </TD> <TD> 0.29 </TD> <TD> 0.59 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 33 </TD> <TD> 6 </TD> <TD> 34 </TD> <TD> 65 </TD> <TD> 0.49 (0.37 to 0.62) </TD> <TD> 0.92 (0.83 to 0.97) </TD> <TD> 0.85 (0.69 to 0.94) </TD> <TD> 0.66 (0.55 to 0.75) </TD> <TD> 5.83 (2.61 to 13.01) </TD> <TD> 0.41 (0.19 to 0.59) </TD> <TD> 0.20 </TD> <TD> 0.62 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 25 </TD> <TD> 4 </TD> <TD> 42 </TD> <TD> 67 </TD> <TD> 0.37 (0.26 to 0.50) </TD> <TD> 0.94 (0.86 to 0.98) </TD> <TD> 0.86 (0.68 to 0.96) </TD> <TD> 0.61 (0.52 to 0.71) </TD> <TD> 6.62 (2.43 to 18.03) </TD> <TD> 0.32 (0.12 to 0.48) </TD> <TD> 0.02 </TD> <TD> 0.59 </TD> </TR>
  <TR> <TD> 13 </TD> <TD> 22 </TD> <TD> 3 </TD> <TD> 45 </TD> <TD> 68 </TD> <TD> 0.33 (0.22 to 0.45) </TD> <TD> 0.96 (0.88 to 0.99) </TD> <TD> 0.88 (0.69 to 0.97) </TD> <TD> 0.60 (0.51 to 0.69) </TD> <TD> 7.77 (2.44 to 24.77) </TD> <TD> 0.29 (0.10 to 0.45) </TD> <TD> -0.03 </TD> <TD> 0.60 </TD> </TR>
  <TR> <TD> 14 </TD> <TD> 18 </TD> <TD> 2 </TD> <TD> 49 </TD> <TD> 69 </TD> <TD> 0.27 (0.17 to 0.39) </TD> <TD> 0.97 (0.90 to 1.00) </TD> <TD> 0.90 (0.68 to 0.99) </TD> <TD> 0.58 (0.49 to 0.67) </TD> <TD> 9.54 (2.30 to 39.54) </TD> <TD> 0.24 (0.07 to 0.39) </TD> <TD> -0.11 </TD> <TD> 0.59 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 9 using the Youden-Index J threshold is 0.64 (95% CI 0.52 to 0.76). Test specificity is 0.82 (95% CI 0.71 to 0.9). The likelihood ratio of a positive test is 3.51 (95% CI 2.08 to 5.91). The number needed to diagnose (NND) is 2.17 (95% CI 1.54 to 4.55). Around 22 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 5 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 11 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails2.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of Hazardous Drinking (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>2. Hazardous Drinking (any) by AUDIT-C Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails3.png) <br><center><b>Figure: Predicting Hazardous Drinking (any) with AUDIT-C score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting Hazardous Drinking (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Hazardous Drinking (any) with AUDIT-C score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 66 </TD> <TD> 55 </TD> <TD> 1 </TD> <TD> 16 </TD> <TD> 0.99 (0.92 to 1.00) </TD> <TD> 0.23 (0.13 to 0.34) </TD> <TD> 0.55 (0.45 to 0.64) </TD> <TD> 0.94 (0.71 to 1.00) </TD> <TD> 1.27 (1.12 to 1.45) </TD> <TD> 0.21 (0.05 to 0.34) </TD> <TD> 0.60 </TD> <TD> -0.16 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 65 </TD> <TD> 46 </TD> <TD> 2 </TD> <TD> 25 </TD> <TD> 0.97 (0.90 to 1.00) </TD> <TD> 0.35 (0.24 to 0.47) </TD> <TD> 0.59 (0.49 to 0.68) </TD> <TD> 0.93 (0.76 to 0.99) </TD> <TD> 1.50 (1.26 to 1.79) </TD> <TD> 0.32 (0.14 to 0.47) </TD> <TD> 0.63 </TD> <TD> 0.01 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 63 </TD> <TD> 35 </TD> <TD> 4 </TD> <TD> 36 </TD> <TD> 0.94 (0.85 to 0.98) </TD> <TD> 0.51 (0.39 to 0.63) </TD> <TD> 0.64 (0.54 to 0.74) </TD> <TD> 0.90 (0.76 to 0.97) </TD> <TD> 1.91 (1.50 to 2.43) </TD> <TD> 0.45 (0.24 to 0.61) </TD> <TD> 0.67 </TD> <TD> 0.23 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 55 </TD> <TD> 22 </TD> <TD> 12 </TD> <TD> 49 </TD> <TD> 0.82 (0.71 to 0.90) </TD> <TD> 0.69 (0.57 to 0.79) </TD> <TD> 0.71 (0.60 to 0.81) </TD> <TD> 0.80 (0.68 to 0.89) </TD> <TD> 2.65 (1.84 to 3.82) </TD> <TD> 0.51 (0.28 to 0.70) </TD> <TD> 0.57 </TD> <TD> 0.44 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 48 </TD> <TD> 15 </TD> <TD> 19 </TD> <TD> 56 </TD> <TD> 0.72 (0.59 to 0.82) </TD> <TD> 0.79 (0.68 to 0.88) </TD> <TD> 0.76 (0.64 to 0.86) </TD> <TD> 0.75 (0.63 to 0.84) </TD> <TD> 3.39 (2.11 to 5.45) </TD> <TD> 0.51 (0.27 to 0.70) </TD> <TD> 0.48 </TD> <TD> 0.54 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 35 </TD> <TD> 5 </TD> <TD> 32 </TD> <TD> 66 </TD> <TD> 0.52 (0.40 to 0.65) </TD> <TD> 0.93 (0.84 to 0.98) </TD> <TD> 0.88 (0.73 to 0.96) </TD> <TD> 0.67 (0.57 to 0.76) </TD> <TD> 7.42 (3.09 to 17.80) </TD> <TD> 0.45 (0.24 to 0.62) </TD> <TD> 0.25 </TD> <TD> 0.66 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 23 </TD> <TD> 3 </TD> <TD> 44 </TD> <TD> 68 </TD> <TD> 0.34 (0.23 to 0.47) </TD> <TD> 0.96 (0.88 to 0.99) </TD> <TD> 0.88 (0.70 to 0.98) </TD> <TD> 0.61 (0.51 to 0.70) </TD> <TD> 8.12 (2.56 to 25.81) </TD> <TD> 0.30 (0.11 to 0.46) </TD> <TD> -0.01 </TD> <TD> 0.61 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 14 </TD> <TD> 2 </TD> <TD> 53 </TD> <TD> 69 </TD> <TD> 0.21 (0.12 to 0.33) </TD> <TD> 0.97 (0.90 to 1.00) </TD> <TD> 0.88 (0.62 to 0.98) </TD> <TD> 0.57 (0.47 to 0.66) </TD> <TD> 7.42 (1.75 to 31.42) </TD> <TD> 0.18 (0.02 to 0.32) </TD> <TD> -0.20 </TD> <TD> 0.56 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 6 </TD> <TD> 1 </TD> <TD> 61 </TD> <TD> 70 </TD> <TD> 0.09 (0.03 to 0.18) </TD> <TD> 0.99 (0.92 to 1.00) </TD> <TD> 0.86 (0.42 to 1.00) </TD> <TD> 0.53 (0.45 to 0.62) </TD> <TD> 6.36 (0.79 to 51.43) </TD> <TD> 0.08 (-0.04 to 0.18) </TD> <TD> -0.37 </TD> <TD> 0.53 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 5 using the Youden-Index J threshold is 0.82 (95% CI 0.71 to 0.9). Test specificity is 0.69 (95% CI 0.57 to 0.79). The likelihood ratio of a positive test is 2.65 (95% CI 1.84 to 3.82). The number needed to diagnose (NND) is 1.96 (95% CI 1.43 to 3.57). Around 20 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 4 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 7 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails4.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of Hazardous Drinking (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>3. Hazardous Drinking (any) by AUDIT Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails5.png) <br><center><b>Figure: Predicting Hazardous Drinking (any) with AUDIT score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting Hazardous Drinking (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Hazardous Drinking (any) with AUDIT score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 144 </TD> <TD> 88 </TD> <TD> 0 </TD> <TD> 50 </TD> <TD> 1.00 (0.96 to 1.00) </TD> <TD> 0.36 (0.28 to 0.45) </TD> <TD> 0.62 (0.55 to 0.68) </TD> <TD> 1.00 (0.90 to 1.00) </TD> <TD> 1.57 (1.38 to 1.78) </TD> <TD> 0.36 (0.24 to 0.45) </TD> <TD> 0.68 </TD> <TD> 0.04 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 137 </TD> <TD> 69 </TD> <TD> 7 </TD> <TD> 69 </TD> <TD> 0.95 (0.90 to 0.98) </TD> <TD> 0.50 (0.41 to 0.59) </TD> <TD> 0.67 (0.60 to 0.73) </TD> <TD> 0.91 (0.82 to 0.96) </TD> <TD> 1.90 (1.60 to 2.26) </TD> <TD> 0.45 (0.32 to 0.57) </TD> <TD> 0.67 </TD> <TD> 0.23 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 127 </TD> <TD> 45 </TD> <TD> 17 </TD> <TD> 93 </TD> <TD> 0.88 (0.82 to 0.93) </TD> <TD> 0.67 (0.59 to 0.75) </TD> <TD> 0.74 (0.67 to 0.80) </TD> <TD> 0.85 (0.76 to 0.91) </TD> <TD> 2.70 (2.11 to 3.46) </TD> <TD> 0.56 (0.41 to 0.68) </TD> <TD> 0.66 </TD> <TD> 0.45 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 109 </TD> <TD> 29 </TD> <TD> 35 </TD> <TD> 109 </TD> <TD> 0.76 (0.68 to 0.82) </TD> <TD> 0.79 (0.71 to 0.85) </TD> <TD> 0.79 (0.71 to 0.85) </TD> <TD> 0.76 (0.68 to 0.82) </TD> <TD> 3.60 (2.57 to 5.04) </TD> <TD> 0.55 (0.39 to 0.68) </TD> <TD> 0.54 </TD> <TD> 0.56 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 94 </TD> <TD> 18 </TD> <TD> 50 </TD> <TD> 120 </TD> <TD> 0.65 (0.57 to 0.73) </TD> <TD> 0.87 (0.80 to 0.92) </TD> <TD> 0.84 (0.76 to 0.90) </TD> <TD> 0.71 (0.63 to 0.77) </TD> <TD> 5.00 (3.20 to 7.82) </TD> <TD> 0.52 (0.37 to 0.65) </TD> <TD> 0.41 </TD> <TD> 0.63 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 82 </TD> <TD> 13 </TD> <TD> 62 </TD> <TD> 125 </TD> <TD> 0.57 (0.48 to 0.65) </TD> <TD> 0.91 (0.84 to 0.95) </TD> <TD> 0.86 (0.78 to 0.93) </TD> <TD> 0.67 (0.60 to 0.74) </TD> <TD> 6.04 (3.53 to 10.34) </TD> <TD> 0.48 (0.33 to 0.60) </TD> <TD> 0.31 </TD> <TD> 0.65 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 66 </TD> <TD> 9 </TD> <TD> 78 </TD> <TD> 129 </TD> <TD> 0.46 (0.38 to 0.54) </TD> <TD> 0.93 (0.88 to 0.97) </TD> <TD> 0.88 (0.78 to 0.94) </TD> <TD> 0.62 (0.55 to 0.69) </TD> <TD> 7.03 (3.65 to 13.54) </TD> <TD> 0.39 (0.25 to 0.51) </TD> <TD> 0.16 </TD> <TD> 0.62 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 52 </TD> <TD> 6 </TD> <TD> 92 </TD> <TD> 132 </TD> <TD> 0.36 (0.28 to 0.45) </TD> <TD> 0.96 (0.91 to 0.98) </TD> <TD> 0.90 (0.79 to 0.96) </TD> <TD> 0.59 (0.52 to 0.65) </TD> <TD> 8.31 (3.69 to 18.71) </TD> <TD> 0.32 (0.19 to 0.43) </TD> <TD> 0.02 </TD> <TD> 0.62 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 4 using the Youden-Index J threshold is 0.88 (95% CI 0.82 to 0.93). Test specificity is 0.67 (95% CI 0.59 to 0.75). The likelihood ratio of a positive test is 2.7 (95% CI 2.11 to 3.46). The number needed to diagnose (NND) is 1.79 (95% CI 1.47 to 2.44). Around 18 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 7 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails6.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of Hazardous Drinking (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>4. Hazardous Drinking (any) by AUDIT-C Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails7.png) <br><center><b>Figure: Predicting Hazardous Drinking (any) with AUDIT-C score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting Hazardous Drinking (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Hazardous Drinking (any) with AUDIT-C score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 144 </TD> <TD> 87 </TD> <TD> 0 </TD> <TD> 51 </TD> <TD> 1.00 (0.96 to 1.00) </TD> <TD> 0.37 (0.29 to 0.46) </TD> <TD> 0.62 (0.56 to 0.69) </TD> <TD> 1.00 (0.90 to 1.00) </TD> <TD> 1.59 (1.40 to 1.80) </TD> <TD> 0.37 (0.25 to 0.46) </TD> <TD> 0.69 </TD> <TD> 0.05 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 134 </TD> <TD> 62 </TD> <TD> 10 </TD> <TD> 76 </TD> <TD> 0.93 (0.88 to 0.97) </TD> <TD> 0.55 (0.46 to 0.64) </TD> <TD> 0.68 (0.61 to 0.75) </TD> <TD> 0.88 (0.80 to 0.94) </TD> <TD> 2.07 (1.71 to 2.50) </TD> <TD> 0.48 (0.34 to 0.60) </TD> <TD> 0.67 </TD> <TD> 0.29 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 118 </TD> <TD> 34 </TD> <TD> 26 </TD> <TD> 104 </TD> <TD> 0.82 (0.75 to 0.88) </TD> <TD> 0.75 (0.67 to 0.82) </TD> <TD> 0.78 (0.70 to 0.84) </TD> <TD> 0.80 (0.72 to 0.86) </TD> <TD> 3.33 (2.46 to 4.50) </TD> <TD> 0.57 (0.42 to 0.70) </TD> <TD> 0.60 </TD> <TD> 0.53 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 93 </TD> <TD> 17 </TD> <TD> 51 </TD> <TD> 121 </TD> <TD> 0.65 (0.56 to 0.72) </TD> <TD> 0.88 (0.81 to 0.93) </TD> <TD> 0.85 (0.76 to 0.91) </TD> <TD> 0.70 (0.63 to 0.77) </TD> <TD> 5.24 (3.31 to 8.32) </TD> <TD> 0.52 (0.37 to 0.65) </TD> <TD> 0.42 </TD> <TD> 0.65 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 68 </TD> <TD> 7 </TD> <TD> 76 </TD> <TD> 131 </TD> <TD> 0.47 (0.39 to 0.56) </TD> <TD> 0.95 (0.90 to 0.98) </TD> <TD> 0.91 (0.82 to 0.96) </TD> <TD> 0.63 (0.56 to 0.70) </TD> <TD> 9.31 (4.43 to 19.55) </TD> <TD> 0.42 (0.29 to 0.54) </TD> <TD> 0.18 </TD> <TD> 0.66 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 47 </TD> <TD> 4 </TD> <TD> 97 </TD> <TD> 134 </TD> <TD> 0.33 (0.25 to 0.41) </TD> <TD> 0.97 (0.93 to 0.99) </TD> <TD> 0.92 (0.81 to 0.98) </TD> <TD> 0.58 (0.51 to 0.64) </TD> <TD> 11.26 (4.17 to 30.42) </TD> <TD> 0.30 (0.18 to 0.40) </TD> <TD> -0.02 </TD> <TD> 0.62 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 23 </TD> <TD> 1 </TD> <TD> 121 </TD> <TD> 137 </TD> <TD> 0.16 (0.10 to 0.23) </TD> <TD> 0.99 (0.96 to 1.00) </TD> <TD> 0.96 (0.79 to 1.00) </TD> <TD> 0.53 (0.47 to 0.59) </TD> <TD> 22.04 (3.02 to 161.00) </TD> <TD> 0.15 (0.06 to 0.23) </TD> <TD> -0.27 </TD> <TD> 0.56 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 11 </TD> <TD> 1 </TD> <TD> 133 </TD> <TD> 137 </TD> <TD> 0.08 (0.04 to 0.13) </TD> <TD> 0.99 (0.96 to 1.00) </TD> <TD> 0.92 (0.62 to 1.00) </TD> <TD> 0.51 (0.45 to 0.57) </TD> <TD> 10.54 (1.38 to 80.57) </TD> <TD> 0.07 (-0.00 to 0.13) </TD> <TD> -0.39 </TD> <TD> 0.52 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 4 using the Youden-Index J threshold is 0.82 (95% CI 0.75 to 0.88). Test specificity is 0.75 (95% CI 0.67 to 0.82). The likelihood ratio of a positive test is 3.33 (95% CI 2.46 to 4.5). The number needed to diagnose (NND) is 1.75 (95% CI 1.43 to 2.38). Around 18 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 6 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails8.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of Hazardous Drinking (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>5. Binge drinker (any) by AUDIT Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails9.png) <br><center><b>Figure: Predicting Binge drinker (any) with AUDIT score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting Binge drinker (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Binge drinker (any) with AUDIT score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 97 </TD> <TD> 27 </TD> <TD> 1 </TD> <TD> 13 </TD> <TD> 0.99 (0.94 to 1.00) </TD> <TD> 0.33 (0.19 to 0.49) </TD> <TD> 0.78 (0.70 to 0.85) </TD> <TD> 0.93 (0.66 to 1.00) </TD> <TD> 1.47 (1.18 to 1.82) </TD> <TD> 0.31 (0.13 to 0.49) </TD> <TD> 0.65 </TD> <TD> -0.01 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 96 </TD> <TD> 20 </TD> <TD> 2 </TD> <TD> 20 </TD> <TD> 0.98 (0.93 to 1.00) </TD> <TD> 0.50 (0.34 to 0.66) </TD> <TD> 0.83 (0.75 to 0.89) </TD> <TD> 0.91 (0.71 to 0.99) </TD> <TD> 1.96 (1.44 to 2.67) </TD> <TD> 0.48 (0.27 to 0.66) </TD> <TD> 0.72 </TD> <TD> 0.24 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 94 </TD> <TD> 18 </TD> <TD> 4 </TD> <TD> 22 </TD> <TD> 0.96 (0.90 to 0.99) </TD> <TD> 0.55 (0.38 to 0.71) </TD> <TD> 0.84 (0.76 to 0.90) </TD> <TD> 0.85 (0.65 to 0.96) </TD> <TD> 2.13 (1.51 to 3.01) </TD> <TD> 0.51 (0.28 to 0.70) </TD> <TD> 0.71 </TD> <TD> 0.31 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 88 </TD> <TD> 11 </TD> <TD> 10 </TD> <TD> 29 </TD> <TD> 0.90 (0.82 to 0.95) </TD> <TD> 0.72 (0.56 to 0.85) </TD> <TD> 0.89 (0.81 to 0.94) </TD> <TD> 0.74 (0.58 to 0.87) </TD> <TD> 3.27 (1.97 to 5.42) </TD> <TD> 0.62 (0.38 to 0.80) </TD> <TD> 0.71 </TD> <TD> 0.53 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 79 </TD> <TD> 7 </TD> <TD> 19 </TD> <TD> 33 </TD> <TD> 0.81 (0.71 to 0.88) </TD> <TD> 0.82 (0.67 to 0.93) </TD> <TD> 0.92 (0.84 to 0.97) </TD> <TD> 0.63 (0.49 to 0.76) </TD> <TD> 4.61 (2.33 to 9.09) </TD> <TD> 0.63 (0.39 to 0.81) </TD> <TD> 0.62 </TD> <TD> 0.64 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 70 </TD> <TD> 3 </TD> <TD> 28 </TD> <TD> 37 </TD> <TD> 0.71 (0.61 to 0.80) </TD> <TD> 0.93 (0.80 to 0.98) </TD> <TD> 0.96 (0.88 to 0.99) </TD> <TD> 0.57 (0.44 to 0.69) </TD> <TD> 9.52 (3.18 to 28.48) </TD> <TD> 0.64 (0.41 to 0.79) </TD> <TD> 0.53 </TD> <TD> 0.75 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 62 </TD> <TD> 2 </TD> <TD> 36 </TD> <TD> 38 </TD> <TD> 0.63 (0.53 to 0.73) </TD> <TD> 0.95 (0.83 to 0.99) </TD> <TD> 0.97 (0.89 to 1.00) </TD> <TD> 0.51 (0.39 to 0.63) </TD> <TD> 12.65 (3.25 to 49.26) </TD> <TD> 0.58 (0.36 to 0.72) </TD> <TD> 0.42 </TD> <TD> 0.74 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 55 </TD> <TD> 1 </TD> <TD> 43 </TD> <TD> 39 </TD> <TD> 0.56 (0.46 to 0.66) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.98 (0.90 to 1.00) </TD> <TD> 0.48 (0.36 to 0.59) </TD> <TD> 22.45 (3.22 to 156.72) </TD> <TD> 0.54 (0.33 to 0.66) </TD> <TD> 0.33 </TD> <TD> 0.74 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 46 </TD> <TD> 1 </TD> <TD> 52 </TD> <TD> 39 </TD> <TD> 0.47 (0.37 to 0.57) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.98 (0.89 to 1.00) </TD> <TD> 0.43 (0.33 to 0.54) </TD> <TD> 18.78 (2.68 to 131.54) </TD> <TD> 0.44 (0.24 to 0.57) </TD> <TD> 0.19 </TD> <TD> 0.69 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 38 </TD> <TD> 1 </TD> <TD> 60 </TD> <TD> 39 </TD> <TD> 0.39 (0.29 to 0.49) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.39 (0.30 to 0.50) </TD> <TD> 15.51 (2.20 to 109.15) </TD> <TD> 0.36 (0.16 to 0.49) </TD> <TD> 0.07 </TD> <TD> 0.65 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 28 </TD> <TD> 1 </TD> <TD> 70 </TD> <TD> 39 </TD> <TD> 0.29 (0.20 to 0.39) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.97 (0.82 to 1.00) </TD> <TD> 0.36 (0.27 to 0.46) </TD> <TD> 11.43 (1.61 to 81.17) </TD> <TD> 0.26 (0.07 to 0.39) </TD> <TD> -0.08 </TD> <TD> 0.60 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 7 using the Youden-Index J threshold is 0.71 (95% CI 0.61 to 0.8). Test specificity is 0.93 (95% CI 0.8 to 0.98). The likelihood ratio of a positive test is 9.52 (95% CI 3.18 to 28.48). The number needed to diagnose (NND) is 1.56 (95% CI 1.27 to 2.44). Around 16 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 3 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 7 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails10.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of Binge drinker (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>6. Binge drinker (any) by AUDIT-C Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails11.png) <br><center><b>Figure: Predicting Binge drinker (any) with AUDIT-C score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting Binge drinker (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Binge drinker (any) with AUDIT-C score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 95 </TD> <TD> 26 </TD> <TD> 3 </TD> <TD> 14 </TD> <TD> 0.97 (0.91 to 0.99) </TD> <TD> 0.35 (0.21 to 0.52) </TD> <TD> 0.79 (0.70 to 0.85) </TD> <TD> 0.82 (0.57 to 0.96) </TD> <TD> 1.49 (1.18 to 1.88) </TD> <TD> 0.32 (0.12 to 0.51) </TD> <TD> 0.63 </TD> <TD> 0.01 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 92 </TD> <TD> 19 </TD> <TD> 6 </TD> <TD> 21 </TD> <TD> 0.94 (0.87 to 0.98) </TD> <TD> 0.53 (0.36 to 0.68) </TD> <TD> 0.83 (0.75 to 0.89) </TD> <TD> 0.78 (0.58 to 0.91) </TD> <TD> 1.98 (1.42 to 2.75) </TD> <TD> 0.46 (0.23 to 0.66) </TD> <TD> 0.67 </TD> <TD> 0.27 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 87 </TD> <TD> 11 </TD> <TD> 11 </TD> <TD> 29 </TD> <TD> 0.89 (0.81 to 0.94) </TD> <TD> 0.72 (0.56 to 0.85) </TD> <TD> 0.89 (0.81 to 0.94) </TD> <TD> 0.72 (0.56 to 0.85) </TD> <TD> 3.23 (1.94 to 5.37) </TD> <TD> 0.61 (0.37 to 0.80) </TD> <TD> 0.69 </TD> <TD> 0.53 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 73 </TD> <TD> 4 </TD> <TD> 25 </TD> <TD> 36 </TD> <TD> 0.74 (0.65 to 0.83) </TD> <TD> 0.90 (0.76 to 0.97) </TD> <TD> 0.95 (0.87 to 0.99) </TD> <TD> 0.59 (0.46 to 0.71) </TD> <TD> 7.45 (2.92 to 19.01) </TD> <TD> 0.64 (0.41 to 0.80) </TD> <TD> 0.56 </TD> <TD> 0.72 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 62 </TD> <TD> 1 </TD> <TD> 36 </TD> <TD> 39 </TD> <TD> 0.63 (0.53 to 0.73) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.98 (0.91 to 1.00) </TD> <TD> 0.52 (0.40 to 0.64) </TD> <TD> 25.31 (3.63 to 176.31) </TD> <TD> 0.61 (0.40 to 0.73) </TD> <TD> 0.43 </TD> <TD> 0.77 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 39 </TD> <TD> 1 </TD> <TD> 59 </TD> <TD> 39 </TD> <TD> 0.40 (0.30 to 0.50) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.40 (0.30 to 0.50) </TD> <TD> 15.92 (2.26 to 111.95) </TD> <TD> 0.37 (0.17 to 0.50) </TD> <TD> 0.08 </TD> <TD> 0.66 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 25 </TD> <TD> 1 </TD> <TD> 73 </TD> <TD> 39 </TD> <TD> 0.26 (0.17 to 0.35) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.96 (0.80 to 1.00) </TD> <TD> 0.35 (0.26 to 0.44) </TD> <TD> 10.20 (1.43 to 72.78) </TD> <TD> 0.23 (0.04 to 0.35) </TD> <TD> -0.12 </TD> <TD> 0.58 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 15 </TD> <TD> 1 </TD> <TD> 83 </TD> <TD> 39 </TD> <TD> 0.15 (0.09 to 0.24) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.94 (0.70 to 1.00) </TD> <TD> 0.32 (0.24 to 0.41) </TD> <TD> 6.12 (0.84 to 44.81) </TD> <TD> 0.13 (-0.04 to 0.24) </TD> <TD> -0.29 </TD> <TD> 0.53 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 6 </TD> <TD> 1 </TD> <TD> 92 </TD> <TD> 39 </TD> <TD> 0.06 (0.02 to 0.13) </TD> <TD> 0.97 (0.87 to 1.00) </TD> <TD> 0.86 (0.42 to 1.00) </TD> <TD> 0.30 (0.22 to 0.38) </TD> <TD> 2.45 (0.30 to 19.70) </TD> <TD> 0.04 (-0.11 to 0.13) </TD> <TD> -0.43 </TD> <TD> 0.49 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 5 using the Youden-Index J threshold is 0.74 (95% CI 0.65 to 0.83). Test specificity is 0.9 (95% CI 0.76 to 0.97). The likelihood ratio of a positive test is 7.45 (95% CI 2.92 to 19.01). The number needed to diagnose (NND) is 1.56 (95% CI 1.25 to 2.44). Around 16 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 4 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 6 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails12.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of Binge drinker (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>7. Binge drinker (any) by AUDIT Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails13.png) <br><center><b>Figure: Predicting Binge drinker (any) with AUDIT score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting Binge drinker (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Binge drinker (any) with AUDIT score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 182 </TD> <TD> 50 </TD> <TD> 3 </TD> <TD> 47 </TD> <TD> 0.98 (0.95 to 1.00) </TD> <TD> 0.48 (0.38 to 0.59) </TD> <TD> 0.78 (0.73 to 0.84) </TD> <TD> 0.94 (0.83 to 0.99) </TD> <TD> 1.91 (1.57 to 2.32) </TD> <TD> 0.47 (0.34 to 0.58) </TD> <TD> 0.71 </TD> <TD> 0.21 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 171 </TD> <TD> 35 </TD> <TD> 14 </TD> <TD> 62 </TD> <TD> 0.92 (0.88 to 0.96) </TD> <TD> 0.64 (0.54 to 0.73) </TD> <TD> 0.83 (0.77 to 0.88) </TD> <TD> 0.82 (0.71 to 0.90) </TD> <TD> 2.56 (1.96 to 3.35) </TD> <TD> 0.56 (0.41 to 0.69) </TD> <TD> 0.70 </TD> <TD> 0.42 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 149 </TD> <TD> 23 </TD> <TD> 36 </TD> <TD> 74 </TD> <TD> 0.81 (0.74 to 0.86) </TD> <TD> 0.76 (0.67 to 0.84) </TD> <TD> 0.87 (0.81 to 0.91) </TD> <TD> 0.67 (0.58 to 0.76) </TD> <TD> 3.40 (2.36 to 4.89) </TD> <TD> 0.57 (0.41 to 0.70) </TD> <TD> 0.60 </TD> <TD> 0.55 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 126 </TD> <TD> 12 </TD> <TD> 59 </TD> <TD> 85 </TD> <TD> 0.68 (0.61 to 0.75) </TD> <TD> 0.88 (0.79 to 0.93) </TD> <TD> 0.91 (0.85 to 0.95) </TD> <TD> 0.59 (0.51 to 0.67) </TD> <TD> 5.51 (3.21 to 9.44) </TD> <TD> 0.56 (0.40 to 0.68) </TD> <TD> 0.46 </TD> <TD> 0.66 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 104 </TD> <TD> 8 </TD> <TD> 81 </TD> <TD> 89 </TD> <TD> 0.56 (0.49 to 0.63) </TD> <TD> 0.92 (0.84 to 0.96) </TD> <TD> 0.93 (0.86 to 0.97) </TD> <TD> 0.52 (0.45 to 0.60) </TD> <TD> 6.82 (3.47 to 13.40) </TD> <TD> 0.48 (0.33 to 0.60) </TD> <TD> 0.30 </TD> <TD> 0.66 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 90 </TD> <TD> 5 </TD> <TD> 95 </TD> <TD> 92 </TD> <TD> 0.49 (0.41 to 0.56) </TD> <TD> 0.95 (0.88 to 0.98) </TD> <TD> 0.95 (0.88 to 0.98) </TD> <TD> 0.49 (0.42 to 0.57) </TD> <TD> 9.44 (3.97 to 22.45) </TD> <TD> 0.43 (0.30 to 0.54) </TD> <TD> 0.21 </TD> <TD> 0.67 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 71 </TD> <TD> 4 </TD> <TD> 114 </TD> <TD> 93 </TD> <TD> 0.38 (0.31 to 0.46) </TD> <TD> 0.96 (0.90 to 0.99) </TD> <TD> 0.95 (0.87 to 0.99) </TD> <TD> 0.45 (0.38 to 0.52) </TD> <TD> 9.31 (3.50 to 24.72) </TD> <TD> 0.34 (0.21 to 0.45) </TD> <TD> 0.05 </TD> <TD> 0.63 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 55 </TD> <TD> 3 </TD> <TD> 130 </TD> <TD> 94 </TD> <TD> 0.30 (0.23 to 0.37) </TD> <TD> 0.97 (0.91 to 0.99) </TD> <TD> 0.95 (0.86 to 0.99) </TD> <TD> 0.42 (0.35 to 0.49) </TD> <TD> 9.61 (3.09 to 29.93) </TD> <TD> 0.27 (0.14 to 0.36) </TD> <TD> -0.07 </TD> <TD> 0.60 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 4 using the Youden-Index J threshold is 0.81 (95% CI 0.74 to 0.86). Test specificity is 0.76 (95% CI 0.67 to 0.84). The likelihood ratio of a positive test is 3.4 (95% CI 2.36 to 4.89). The number needed to diagnose (NND) is 1.75 (95% CI 1.43 to 2.44). Around 18 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 7 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails14.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of Binge drinker (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>8. Binge drinker (any) by AUDIT-C Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails15.png) <br><center><b>Figure: Predicting Binge drinker (any) with AUDIT-C score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting Binge drinker (any). The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting Binge drinker (any) with AUDIT-C score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 182 </TD> <TD> 49 </TD> <TD> 3 </TD> <TD> 48 </TD> <TD> 0.98 (0.95 to 1.00) </TD> <TD> 0.49 (0.39 to 0.60) </TD> <TD> 0.79 (0.73 to 0.84) </TD> <TD> 0.94 (0.84 to 0.99) </TD> <TD> 1.95 (1.60 to 2.37) </TD> <TD> 0.48 (0.35 to 0.59) </TD> <TD> 0.71 </TD> <TD> 0.23 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 165 </TD> <TD> 31 </TD> <TD> 20 </TD> <TD> 66 </TD> <TD> 0.89 (0.84 to 0.93) </TD> <TD> 0.68 (0.58 to 0.77) </TD> <TD> 0.84 (0.78 to 0.89) </TD> <TD> 0.77 (0.66 to 0.85) </TD> <TD> 2.79 (2.08 to 3.75) </TD> <TD> 0.57 (0.42 to 0.70) </TD> <TD> 0.68 </TD> <TD> 0.47 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 135 </TD> <TD> 17 </TD> <TD> 50 </TD> <TD> 80 </TD> <TD> 0.73 (0.66 to 0.79) </TD> <TD> 0.82 (0.73 to 0.89) </TD> <TD> 0.89 (0.83 to 0.93) </TD> <TD> 0.62 (0.53 to 0.70) </TD> <TD> 4.16 (2.68 to 6.47) </TD> <TD> 0.55 (0.39 to 0.69) </TD> <TD> 0.50 </TD> <TD> 0.59 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 102 </TD> <TD> 8 </TD> <TD> 83 </TD> <TD> 89 </TD> <TD> 0.55 (0.48 to 0.62) </TD> <TD> 0.92 (0.84 to 0.96) </TD> <TD> 0.93 (0.86 to 0.97) </TD> <TD> 0.52 (0.44 to 0.59) </TD> <TD> 6.69 (3.40 to 13.15) </TD> <TD> 0.47 (0.32 to 0.59) </TD> <TD> 0.29 </TD> <TD> 0.66 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 72 </TD> <TD> 3 </TD> <TD> 113 </TD> <TD> 94 </TD> <TD> 0.39 (0.32 to 0.46) </TD> <TD> 0.97 (0.91 to 0.99) </TD> <TD> 0.96 (0.89 to 0.99) </TD> <TD> 0.45 (0.38 to 0.52) </TD> <TD> 12.58 (4.07 to 38.90) </TD> <TD> 0.36 (0.23 to 0.46) </TD> <TD> 0.07 </TD> <TD> 0.65 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 49 </TD> <TD> 2 </TD> <TD> 136 </TD> <TD> 95 </TD> <TD> 0.26 (0.20 to 0.33) </TD> <TD> 0.98 (0.93 to 1.00) </TD> <TD> 0.96 (0.87 to 1.00) </TD> <TD> 0.41 (0.35 to 0.48) </TD> <TD> 12.85 (3.19 to 51.70) </TD> <TD> 0.24 (0.13 to 0.33) </TD> <TD> -0.12 </TD> <TD> 0.60 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 23 </TD> <TD> 1 </TD> <TD> 162 </TD> <TD> 96 </TD> <TD> 0.12 (0.08 to 0.18) </TD> <TD> 0.99 (0.94 to 1.00) </TD> <TD> 0.96 (0.79 to 1.00) </TD> <TD> 0.37 (0.31 to 0.43) </TD> <TD> 12.06 (1.65 to 87.96) </TD> <TD> 0.11 (0.02 to 0.18) </TD> <TD> -0.32 </TD> <TD> 0.54 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 3 using the Youden-Index J threshold is 0.89 (95% CI 0.84 to 0.93). Test specificity is 0.68 (95% CI 0.58 to 0.77). The likelihood ratio of a positive test is 2.79 (95% CI 2.08 to 3.75). The number needed to diagnose (NND) is 1.75 (95% CI 1.43 to 2.38). Around 18 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 5 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails16.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of Binge drinker (any), for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>9. DSM-IV Alcohol Dependence by AUDIT Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails17.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Dependence with AUDIT score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-IV Alcohol Dependence. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Dependence with AUDIT score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 7 </TD> <TD> 14 </TD> <TD> 59 </TD> <TD> 4 </TD> <TD> 61 </TD> <TD> 0.78 (0.52 to 0.94) </TD> <TD> 0.51 (0.42 to 0.60) </TD> <TD> 0.19 (0.11 to 0.30) </TD> <TD> 0.94 (0.85 to 0.98) </TD> <TD> 1.58 (1.16 to 2.15) </TD> <TD> 0.29 (-0.06 to 0.54) </TD> <TD> 0.42 </TD> <TD> 0.16 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 14 </TD> <TD> 50 </TD> <TD> 4 </TD> <TD> 70 </TD> <TD> 0.78 (0.52 to 0.94) </TD> <TD> 0.58 (0.49 to 0.67) </TD> <TD> 0.22 (0.13 to 0.34) </TD> <TD> 0.95 (0.87 to 0.99) </TD> <TD> 1.87 (1.35 to 2.58) </TD> <TD> 0.36 (0.01 to 0.61) </TD> <TD> 0.46 </TD> <TD> 0.26 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 14 </TD> <TD> 42 </TD> <TD> 4 </TD> <TD> 78 </TD> <TD> 0.78 (0.52 to 0.94) </TD> <TD> 0.65 (0.56 to 0.73) </TD> <TD> 0.25 (0.14 to 0.38) </TD> <TD> 0.95 (0.88 to 0.99) </TD> <TD> 2.22 (1.57 to 3.14) </TD> <TD> 0.43 (0.08 to 0.67) </TD> <TD> 0.49 </TD> <TD> 0.37 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 13 </TD> <TD> 34 </TD> <TD> 5 </TD> <TD> 86 </TD> <TD> 0.72 (0.47 to 0.90) </TD> <TD> 0.72 (0.63 to 0.80) </TD> <TD> 0.28 (0.16 to 0.43) </TD> <TD> 0.95 (0.88 to 0.98) </TD> <TD> 2.55 (1.70 to 3.82) </TD> <TD> 0.44 (0.09 to 0.70) </TD> <TD> 0.44 </TD> <TD> 0.44 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 12 </TD> <TD> 27 </TD> <TD> 6 </TD> <TD> 93 </TD> <TD> 0.67 (0.41 to 0.87) </TD> <TD> 0.78 (0.69 to 0.85) </TD> <TD> 0.31 (0.17 to 0.48) </TD> <TD> 0.94 (0.87 to 0.98) </TD> <TD> 2.96 (1.86 to 4.72) </TD> <TD> 0.44 (0.10 to 0.71) </TD> <TD> 0.40 </TD> <TD> 0.50 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 12 </TD> <TD> 17 </TD> <TD> 6 </TD> <TD> 103 </TD> <TD> 0.67 (0.41 to 0.87) </TD> <TD> 0.86 (0.78 to 0.92) </TD> <TD> 0.41 (0.24 to 0.61) </TD> <TD> 0.94 (0.88 to 0.98) </TD> <TD> 4.71 (2.72 to 8.14) </TD> <TD> 0.52 (0.19 to 0.78) </TD> <TD> 0.44 </TD> <TD> 0.62 </TD> </TR>
  <TR> <TD> 13 </TD> <TD> 11 </TD> <TD> 14 </TD> <TD> 7 </TD> <TD> 106 </TD> <TD> 0.61 (0.36 to 0.83) </TD> <TD> 0.88 (0.81 to 0.93) </TD> <TD> 0.44 (0.24 to 0.65) </TD> <TD> 0.94 (0.88 to 0.97) </TD> <TD> 5.24 (2.83 to 9.69) </TD> <TD> 0.49 (0.17 to 0.76) </TD> <TD> 0.35 </TD> <TD> 0.62 </TD> </TR>
  <TR> <TD> 14 </TD> <TD> 9 </TD> <TD> 11 </TD> <TD> 9 </TD> <TD> 109 </TD> <TD> 0.50 (0.26 to 0.74) </TD> <TD> 0.91 (0.84 to 0.95) </TD> <TD> 0.45 (0.23 to 0.68) </TD> <TD> 0.92 (0.86 to 0.96) </TD> <TD> 5.45 (2.63 to 11.30) </TD> <TD> 0.41 (0.10 to 0.69) </TD> <TD> 0.21 </TD> <TD> 0.61 </TD> </TR>
  <TR> <TD> 15 </TD> <TD> 8 </TD> <TD> 10 </TD> <TD> 10 </TD> <TD> 110 </TD> <TD> 0.44 (0.22 to 0.69) </TD> <TD> 0.92 (0.85 to 0.96) </TD> <TD> 0.44 (0.22 to 0.69) </TD> <TD> 0.92 (0.85 to 0.96) </TD> <TD> 5.33 (2.43 to 11.71) </TD> <TD> 0.36 (0.07 to 0.65) </TD> <TD> 0.12 </TD> <TD> 0.60 </TD> </TR>
  <TR> <TD> 16 </TD> <TD> 6 </TD> <TD> 8 </TD> <TD> 12 </TD> <TD> 112 </TD> <TD> 0.33 (0.13 to 0.59) </TD> <TD> 0.93 (0.87 to 0.97) </TD> <TD> 0.43 (0.18 to 0.71) </TD> <TD> 0.90 (0.84 to 0.95) </TD> <TD> 5.00 (1.96 to 12.74) </TD> <TD> 0.27 (0.01 to 0.56) </TD> <TD> -0.04 </TD> <TD> 0.56 </TD> </TR>
  <TR> <TD> 17 </TD> <TD> 5 </TD> <TD> 6 </TD> <TD> 13 </TD> <TD> 114 </TD> <TD> 0.28 (0.10 to 0.53) </TD> <TD> 0.95 (0.89 to 0.98) </TD> <TD> 0.45 (0.17 to 0.77) </TD> <TD> 0.90 (0.83 to 0.94) </TD> <TD> 5.56 (1.89 to 16.33) </TD> <TD> 0.23 (-0.01 to 0.52) </TD> <TD> -0.10 </TD> <TD> 0.56 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 12 using the Youden-Index J threshold is 0.67 (95% CI 0.41 to 0.87). Test specificity is 0.86 (95% CI 0.78 to 0.92). The likelihood ratio of a positive test is 4.71 (95% CI 2.72 to 8.14). The number needed to diagnose (NND) is 1.92 (95% CI 1.28 to 5.26). Around 20 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 9 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 12 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails18.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-IV Alcohol Dependence, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>10. DSM-IV Alcohol Dependence by AUDIT-C Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails19.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Dependence with AUDIT-C score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-IV Alcohol Dependence. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Dependence with AUDIT-C score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 17 </TD> <TD> 104 </TD> <TD> 1 </TD> <TD> 16 </TD> <TD> 0.94 (0.73 to 1.00) </TD> <TD> 0.13 (0.08 to 0.21) </TD> <TD> 0.14 (0.08 to 0.22) </TD> <TD> 0.94 (0.71 to 1.00) </TD> <TD> 1.09 (0.95 to 1.24) </TD> <TD> 0.08 (-0.19 to 0.21) </TD> <TD> 0.47 </TD> <TD> -0.33 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 17 </TD> <TD> 94 </TD> <TD> 1 </TD> <TD> 26 </TD> <TD> 0.94 (0.73 to 1.00) </TD> <TD> 0.22 (0.15 to 0.30) </TD> <TD> 0.15 (0.09 to 0.23) </TD> <TD> 0.96 (0.81 to 1.00) </TD> <TD> 1.21 (1.04 to 1.40) </TD> <TD> 0.16 (-0.13 to 0.30) </TD> <TD> 0.52 </TD> <TD> -0.20 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 16 </TD> <TD> 82 </TD> <TD> 2 </TD> <TD> 38 </TD> <TD> 0.89 (0.65 to 0.99) </TD> <TD> 0.32 (0.23 to 0.41) </TD> <TD> 0.16 (0.10 to 0.25) </TD> <TD> 0.95 (0.83 to 0.99) </TD> <TD> 1.30 (1.06 to 1.59) </TD> <TD> 0.21 (-0.11 to 0.39) </TD> <TD> 0.49 </TD> <TD> -0.07 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 14 </TD> <TD> 63 </TD> <TD> 4 </TD> <TD> 57 </TD> <TD> 0.78 (0.52 to 0.94) </TD> <TD> 0.47 (0.38 to 0.57) </TD> <TD> 0.18 (0.10 to 0.29) </TD> <TD> 0.93 (0.84 to 0.98) </TD> <TD> 1.48 (1.10 to 2.00) </TD> <TD> 0.25 (-0.09 to 0.50) </TD> <TD> 0.40 </TD> <TD> 0.09 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 13 </TD> <TD> 50 </TD> <TD> 5 </TD> <TD> 70 </TD> <TD> 0.72 (0.47 to 0.90) </TD> <TD> 0.58 (0.49 to 0.67) </TD> <TD> 0.21 (0.11 to 0.33) </TD> <TD> 0.93 (0.85 to 0.98) </TD> <TD> 1.73 (1.21 to 2.48) </TD> <TD> 0.31 (-0.04 to 0.58) </TD> <TD> 0.37 </TD> <TD> 0.23 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 10 </TD> <TD> 30 </TD> <TD> 8 </TD> <TD> 90 </TD> <TD> 0.56 (0.31 to 0.78) </TD> <TD> 0.75 (0.66 to 0.82) </TD> <TD> 0.25 (0.13 to 0.41) </TD> <TD> 0.92 (0.85 to 0.96) </TD> <TD> 2.22 (1.33 to 3.72) </TD> <TD> 0.31 (-0.03 to 0.61) </TD> <TD> 0.22 </TD> <TD> 0.41 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 7 </TD> <TD> 19 </TD> <TD> 11 </TD> <TD> 101 </TD> <TD> 0.39 (0.17 to 0.64) </TD> <TD> 0.84 (0.76 to 0.90) </TD> <TD> 0.27 (0.12 to 0.48) </TD> <TD> 0.90 (0.83 to 0.95) </TD> <TD> 2.46 (1.21 to 5.00) </TD> <TD> 0.23 (-0.06 to 0.54) </TD> <TD> 0.00 </TD> <TD> 0.46 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 4 </TD> <TD> 12 </TD> <TD> 14 </TD> <TD> 108 </TD> <TD> 0.22 (0.06 to 0.48) </TD> <TD> 0.90 (0.83 to 0.95) </TD> <TD> 0.25 (0.07 to 0.52) </TD> <TD> 0.89 (0.81 to 0.94) </TD> <TD> 2.22 (0.80 to 6.15) </TD> <TD> 0.12 (-0.10 to 0.42) </TD> <TD> -0.22 </TD> <TD> 0.46 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 2 </TD> <TD> 5 </TD> <TD> 16 </TD> <TD> 115 </TD> <TD> 0.11 (0.01 to 0.35) </TD> <TD> 0.96 (0.91 to 0.99) </TD> <TD> 0.29 (0.04 to 0.71) </TD> <TD> 0.88 (0.81 to 0.93) </TD> <TD> 2.67 (0.56 to 12.73) </TD> <TD> 0.07 (-0.08 to 0.33) </TD> <TD> -0.35 </TD> <TD> 0.49 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 2 </TD> <TD> 0 </TD> <TD> 16 </TD> <TD> 120 </TD> <TD> 0.11 (0.01 to 0.35) </TD> <TD> 1.00 (0.95 to 1.00) </TD> <TD> 1.00 (0.09 to 1.00) </TD> <TD> 0.88 (0.82 to 0.93) </TD> <TD> Inf (NaN to Inf) </TD> <TD> 0.11 (-0.03 to 0.35) </TD> <TD> -0.33 </TD> <TD> 0.55 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 6 using the Youden-Index J threshold is 0.72 (95% CI 0.47 to 0.9). Test specificity is 0.58 (95% CI 0.49 to 0.67). The likelihood ratio of a positive test is 1.73 (95% CI 1.21 to 2.48). The number needed to diagnose (NND) is 3.23 (95% CI 1.72 to -25.00). Around 33 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 3 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 11 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails20.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-IV Alcohol Dependence, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>11. DSM-IV Alcohol Dependence by AUDIT Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails21.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Dependence with AUDIT score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-IV Alcohol Dependence. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Dependence with AUDIT score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 24 </TD> <TD> 208 </TD> <TD> 0 </TD> <TD> 50 </TD> <TD> 1.00 (0.80 to 1.00) </TD> <TD> 0.19 (0.15 to 0.25) </TD> <TD> 0.10 (0.07 to 0.15) </TD> <TD> 1.00 (0.90 to 1.00) </TD> <TD> 1.24 (1.17 to 1.32) </TD> <TD> 0.19 (-0.06 to 0.25) </TD> <TD> 0.59 </TD> <TD> -0.21 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 22 </TD> <TD> 184 </TD> <TD> 2 </TD> <TD> 74 </TD> <TD> 0.92 (0.73 to 0.99) </TD> <TD> 0.29 (0.23 to 0.35) </TD> <TD> 0.11 (0.07 to 0.16) </TD> <TD> 0.97 (0.91 to 1.00) </TD> <TD> 1.29 (1.11 to 1.48) </TD> <TD> 0.20 (-0.04 to 0.34) </TD> <TD> 0.53 </TD> <TD> -0.10 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 21 </TD> <TD> 151 </TD> <TD> 3 </TD> <TD> 107 </TD> <TD> 0.88 (0.68 to 0.97) </TD> <TD> 0.41 (0.35 to 0.48) </TD> <TD> 0.12 (0.08 to 0.18) </TD> <TD> 0.97 (0.92 to 0.99) </TD> <TD> 1.50 (1.25 to 1.79) </TD> <TD> 0.29 (0.03 to 0.45) </TD> <TD> 0.53 </TD> <TD> 0.05 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 19 </TD> <TD> 119 </TD> <TD> 5 </TD> <TD> 139 </TD> <TD> 0.79 (0.58 to 0.93) </TD> <TD> 0.54 (0.48 to 0.60) </TD> <TD> 0.14 (0.08 to 0.21) </TD> <TD> 0.97 (0.92 to 0.99) </TD> <TD> 1.72 (1.34 to 2.19) </TD> <TD> 0.33 (0.05 to 0.53) </TD> <TD> 0.46 </TD> <TD> 0.21 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 18 </TD> <TD> 94 </TD> <TD> 6 </TD> <TD> 164 </TD> <TD> 0.75 (0.53 to 0.90) </TD> <TD> 0.64 (0.57 to 0.69) </TD> <TD> 0.16 (0.10 to 0.24) </TD> <TD> 0.96 (0.92 to 0.99) </TD> <TD> 2.06 (1.55 to 2.73) </TD> <TD> 0.39 (0.11 to 0.60) </TD> <TD> 0.45 </TD> <TD> 0.33 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 17 </TD> <TD> 78 </TD> <TD> 7 </TD> <TD> 180 </TD> <TD> 0.71 (0.49 to 0.87) </TD> <TD> 0.70 (0.64 to 0.75) </TD> <TD> 0.18 (0.11 to 0.27) </TD> <TD> 0.96 (0.92 to 0.98) </TD> <TD> 2.34 (1.71 to 3.22) </TD> <TD> 0.41 (0.13 to 0.63) </TD> <TD> 0.42 </TD> <TD> 0.40 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 15 </TD> <TD> 60 </TD> <TD> 9 </TD> <TD> 198 </TD> <TD> 0.62 (0.41 to 0.81) </TD> <TD> 0.77 (0.71 to 0.82) </TD> <TD> 0.20 (0.12 to 0.31) </TD> <TD> 0.96 (0.92 to 0.98) </TD> <TD> 2.69 (1.84 to 3.93) </TD> <TD> 0.39 (0.12 to 0.63) </TD> <TD> 0.31 </TD> <TD> 0.47 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 12 </TD> <TD> 46 </TD> <TD> 12 </TD> <TD> 212 </TD> <TD> 0.50 (0.29 to 0.71) </TD> <TD> 0.82 (0.77 to 0.87) </TD> <TD> 0.21 (0.11 to 0.33) </TD> <TD> 0.95 (0.91 to 0.97) </TD> <TD> 2.80 (1.74 to 4.52) </TD> <TD> 0.32 (0.06 to 0.58) </TD> <TD> 0.16 </TD> <TD> 0.48 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 12 </TD> <TD> 33 </TD> <TD> 12 </TD> <TD> 225 </TD> <TD> 0.50 (0.29 to 0.71) </TD> <TD> 0.87 (0.83 to 0.91) </TD> <TD> 0.27 (0.15 to 0.42) </TD> <TD> 0.95 (0.91 to 0.97) </TD> <TD> 3.91 (2.34 to 6.52) </TD> <TD> 0.37 (0.12 to 0.62) </TD> <TD> 0.19 </TD> <TD> 0.55 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 11 </TD> <TD> 26 </TD> <TD> 13 </TD> <TD> 232 </TD> <TD> 0.46 (0.26 to 0.67) </TD> <TD> 0.90 (0.86 to 0.93) </TD> <TD> 0.30 (0.16 to 0.47) </TD> <TD> 0.95 (0.91 to 0.97) </TD> <TD> 4.55 (2.58 to 8.02) </TD> <TD> 0.36 (0.11 to 0.60) </TD> <TD> 0.14 </TD> <TD> 0.58 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 8 </TD> <TD> 18 </TD> <TD> 16 </TD> <TD> 240 </TD> <TD> 0.33 (0.16 to 0.55) </TD> <TD> 0.93 (0.89 to 0.96) </TD> <TD> 0.31 (0.14 to 0.52) </TD> <TD> 0.94 (0.90 to 0.96) </TD> <TD> 4.78 (2.33 to 9.82) </TD> <TD> 0.26 (0.05 to 0.51) </TD> <TD> -0.04 </TD> <TD> 0.56 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 7 using the Youden-Index J threshold is 0.71 (95% CI 0.49 to 0.87). Test specificity is 0.7 (95% CI 0.64 to 0.75). The likelihood ratio of a positive test is 2.34 (95% CI 1.71 to 3.22). The number needed to diagnose (NND) is 2.44 (95% CI 1.59 to 7.69). Around 25 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 11 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails22.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-IV Alcohol Dependence, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>12. DSM-IV Alcohol Dependence by AUDIT-C Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails23.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Dependence with AUDIT-C score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-IV Alcohol Dependence. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Dependence with AUDIT-C score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 23 </TD> <TD> 208 </TD> <TD> 1 </TD> <TD> 50 </TD> <TD> 0.96 (0.79 to 1.00) </TD> <TD> 0.19 (0.15 to 0.25) </TD> <TD> 0.10 (0.06 to 0.15) </TD> <TD> 0.98 (0.90 to 1.00) </TD> <TD> 1.19 (1.07 to 1.32) </TD> <TD> 0.15 (-0.06 to 0.25) </TD> <TD> 0.53 </TD> <TD> -0.23 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 21 </TD> <TD> 175 </TD> <TD> 3 </TD> <TD> 83 </TD> <TD> 0.88 (0.68 to 0.97) </TD> <TD> 0.32 (0.27 to 0.38) </TD> <TD> 0.11 (0.07 to 0.16) </TD> <TD> 0.97 (0.90 to 0.99) </TD> <TD> 1.29 (1.09 to 1.53) </TD> <TD> 0.20 (-0.06 to 0.36) </TD> <TD> 0.48 </TD> <TD> -0.08 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 19 </TD> <TD> 133 </TD> <TD> 5 </TD> <TD> 125 </TD> <TD> 0.79 (0.58 to 0.93) </TD> <TD> 0.48 (0.42 to 0.55) </TD> <TD> 0.12 (0.08 to 0.19) </TD> <TD> 0.96 (0.91 to 0.99) </TD> <TD> 1.54 (1.21 to 1.95) </TD> <TD> 0.28 (0.00 to 0.48) </TD> <TD> 0.43 </TD> <TD> 0.11 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 15 </TD> <TD> 95 </TD> <TD> 9 </TD> <TD> 163 </TD> <TD> 0.62 (0.41 to 0.81) </TD> <TD> 0.63 (0.57 to 0.69) </TD> <TD> 0.14 (0.08 to 0.21) </TD> <TD> 0.95 (0.90 to 0.98) </TD> <TD> 1.70 (1.20 to 2.41) </TD> <TD> 0.26 (-0.02 to 0.50) </TD> <TD> 0.24 </TD> <TD> 0.26 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 14 </TD> <TD> 61 </TD> <TD> 10 </TD> <TD> 197 </TD> <TD> 0.58 (0.37 to 0.78) </TD> <TD> 0.76 (0.71 to 0.81) </TD> <TD> 0.19 (0.11 to 0.29) </TD> <TD> 0.95 (0.91 to 0.98) </TD> <TD> 2.47 (1.65 to 3.69) </TD> <TD> 0.35 (0.07 to 0.59) </TD> <TD> 0.25 </TD> <TD> 0.43 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 13 </TD> <TD> 38 </TD> <TD> 11 </TD> <TD> 220 </TD> <TD> 0.54 (0.33 to 0.74) </TD> <TD> 0.85 (0.80 to 0.89) </TD> <TD> 0.25 (0.14 to 0.40) </TD> <TD> 0.95 (0.92 to 0.98) </TD> <TD> 3.68 (2.30 to 5.89) </TD> <TD> 0.39 (0.13 to 0.64) </TD> <TD> 0.24 </TD> <TD> 0.54 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 6 </TD> <TD> 18 </TD> <TD> 18 </TD> <TD> 240 </TD> <TD> 0.25 (0.10 to 0.47) </TD> <TD> 0.93 (0.89 to 0.96) </TD> <TD> 0.25 (0.10 to 0.47) </TD> <TD> 0.93 (0.89 to 0.96) </TD> <TD> 3.58 (1.57 to 8.17) </TD> <TD> 0.18 (-0.01 to 0.43) </TD> <TD> -0.16 </TD> <TD> 0.52 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 3 </TD> <TD> 9 </TD> <TD> 21 </TD> <TD> 249 </TD> <TD> 0.12 (0.03 to 0.32) </TD> <TD> 0.97 (0.93 to 0.98) </TD> <TD> 0.25 (0.05 to 0.57) </TD> <TD> 0.92 (0.88 to 0.95) </TD> <TD> 3.58 (1.04 to 12.36) </TD> <TD> 0.09 (-0.04 to 0.31) </TD> <TD> -0.33 </TD> <TD> 0.52 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 1 </TD> <TD> 2 </TD> <TD> 23 </TD> <TD> 256 </TD> <TD> 0.04 (0.00 to 0.21) </TD> <TD> 0.99 (0.97 to 1.00) </TD> <TD> 0.33 (0.01 to 0.91) </TD> <TD> 0.92 (0.88 to 0.95) </TD> <TD> 5.37 (0.51 to 57.14) </TD> <TD> 0.03 (-0.03 to 0.21) </TD> <TD> -0.45 </TD> <TD> 0.50 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 0 </TD> <TD> 0 </TD> <TD> 24 </TD> <TD> 258 </TD> <TD> 0.00 (0.00 to 0.20) </TD> <TD> 1.00 (0.98 to 1.00) </TD> <TD> NaN (0.00 to 1.00) </TD> <TD> 0.91 (0.88 to 0.94) </TD> <TD> NaN (NaN to NaN) </TD> <TD> 0.00 (-0.02 to 0.20) </TD> <TD> -0.50 </TD> <TD> 0.50 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 0 </TD> <TD> 0 </TD> <TD> 24 </TD> <TD> 258 </TD> <TD> 0.00 (0.00 to 0.20) </TD> <TD> 1.00 (0.98 to 1.00) </TD> <TD> NaN (0.00 to 1.00) </TD> <TD> 0.91 (0.88 to 0.94) </TD> <TD> NaN (NaN to NaN) </TD> <TD> 0.00 (-0.02 to 0.20) </TD> <TD> -0.50 </TD> <TD> 0.50 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 7 using the Youden-Index J threshold is 0.54 (95% CI 0.33 to 0.74). Test specificity is 0.85 (95% CI 0.8 to 0.89). The likelihood ratio of a positive test is 3.68 (95% CI 2.3 to 5.89). The number needed to diagnose (NND) is 2.56 (95% CI 1.56 to 7.69). Around 26 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 7 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails24.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-IV Alcohol Dependence, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>13. DSM-IV Alcohol Abuse by AUDIT Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails25.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Abuse with AUDIT score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-IV Alcohol Abuse. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Abuse with AUDIT score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 5 </TD> <TD> 39 </TD> <TD> 60 </TD> <TD> 10 </TD> <TD> 29 </TD> <TD> 0.80 (0.66 to 0.90) </TD> <TD> 0.33 (0.23 to 0.43) </TD> <TD> 0.39 (0.30 to 0.50) </TD> <TD> 0.74 (0.58 to 0.87) </TD> <TD> 1.18 (0.96 to 1.45) </TD> <TD> 0.12 (-0.11 to 0.33) </TD> <TD> 0.37 </TD> <TD> -0.10 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 35 </TD> <TD> 51 </TD> <TD> 14 </TD> <TD> 38 </TD> <TD> 0.71 (0.57 to 0.83) </TD> <TD> 0.43 (0.32 to 0.54) </TD> <TD> 0.41 (0.30 to 0.52) </TD> <TD> 0.73 (0.59 to 0.84) </TD> <TD> 1.25 (0.97 to 1.60) </TD> <TD> 0.14 (-0.11 to 0.37) </TD> <TD> 0.28 </TD> <TD> 0.00 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 30 </TD> <TD> 43 </TD> <TD> 19 </TD> <TD> 46 </TD> <TD> 0.61 (0.46 to 0.75) </TD> <TD> 0.52 (0.41 to 0.62) </TD> <TD> 0.41 (0.30 to 0.53) </TD> <TD> 0.71 (0.58 to 0.81) </TD> <TD> 1.27 (0.93 to 1.73) </TD> <TD> 0.13 (-0.13 to 0.37) </TD> <TD> 0.18 </TD> <TD> 0.08 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 29 </TD> <TD> 35 </TD> <TD> 20 </TD> <TD> 54 </TD> <TD> 0.59 (0.44 to 0.73) </TD> <TD> 0.61 (0.50 to 0.71) </TD> <TD> 0.45 (0.33 to 0.58) </TD> <TD> 0.73 (0.61 to 0.83) </TD> <TD> 1.50 (1.06 to 2.13) </TD> <TD> 0.20 (-0.06 to 0.44) </TD> <TD> 0.19 </TD> <TD> 0.21 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 27 </TD> <TD> 29 </TD> <TD> 22 </TD> <TD> 60 </TD> <TD> 0.55 (0.40 to 0.69) </TD> <TD> 0.67 (0.57 to 0.77) </TD> <TD> 0.48 (0.35 to 0.62) </TD> <TD> 0.73 (0.62 to 0.82) </TD> <TD> 1.69 (1.14 to 2.50) </TD> <TD> 0.23 (-0.03 to 0.46) </TD> <TD> 0.16 </TD> <TD> 0.28 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 24 </TD> <TD> 23 </TD> <TD> 25 </TD> <TD> 66 </TD> <TD> 0.49 (0.34 to 0.64) </TD> <TD> 0.74 (0.64 to 0.83) </TD> <TD> 0.51 (0.36 to 0.66) </TD> <TD> 0.73 (0.62 to 0.81) </TD> <TD> 1.90 (1.20 to 2.98) </TD> <TD> 0.23 (-0.02 to 0.47) </TD> <TD> 0.10 </TD> <TD> 0.35 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 19 </TD> <TD> 20 </TD> <TD> 30 </TD> <TD> 69 </TD> <TD> 0.39 (0.25 to 0.54) </TD> <TD> 0.78 (0.67 to 0.86) </TD> <TD> 0.49 (0.32 to 0.65) </TD> <TD> 0.70 (0.60 to 0.79) </TD> <TD> 1.73 (1.02 to 2.91) </TD> <TD> 0.16 (-0.07 to 0.39) </TD> <TD> -0.03 </TD> <TD> 0.36 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 15 </TD> <TD> 14 </TD> <TD> 34 </TD> <TD> 75 </TD> <TD> 0.31 (0.18 to 0.45) </TD> <TD> 0.84 (0.75 to 0.91) </TD> <TD> 0.52 (0.33 to 0.71) </TD> <TD> 0.69 (0.59 to 0.77) </TD> <TD> 1.95 (1.03 to 3.69) </TD> <TD> 0.15 (-0.07 to 0.37) </TD> <TD> -0.11 </TD> <TD> 0.42 </TD> </TR>
  <TR> <TD> 13 </TD> <TD> 13 </TD> <TD> 12 </TD> <TD> 36 </TD> <TD> 77 </TD> <TD> 0.27 (0.15 to 0.41) </TD> <TD> 0.87 (0.78 to 0.93) </TD> <TD> 0.52 (0.31 to 0.72) </TD> <TD> 0.68 (0.59 to 0.77) </TD> <TD> 1.97 (0.97 to 3.97) </TD> <TD> 0.13 (-0.07 to 0.34) </TD> <TD> -0.16 </TD> <TD> 0.44 </TD> </TR>
  <TR> <TD> 14 </TD> <TD> 10 </TD> <TD> 10 </TD> <TD> 39 </TD> <TD> 79 </TD> <TD> 0.20 (0.10 to 0.34) </TD> <TD> 0.89 (0.80 to 0.94) </TD> <TD> 0.50 (0.27 to 0.73) </TD> <TD> 0.67 (0.58 to 0.75) </TD> <TD> 1.82 (0.81 to 4.06) </TD> <TD> 0.09 (-0.09 to 0.29) </TD> <TD> -0.25 </TD> <TD> 0.44 </TD> </TR>
  <TR> <TD> 15 </TD> <TD> 9 </TD> <TD> 9 </TD> <TD> 40 </TD> <TD> 80 </TD> <TD> 0.18 (0.09 to 0.32) </TD> <TD> 0.90 (0.82 to 0.95) </TD> <TD> 0.50 (0.26 to 0.74) </TD> <TD> 0.67 (0.57 to 0.75) </TD> <TD> 1.82 (0.77 to 4.27) </TD> <TD> 0.08 (-0.10 to 0.27) </TD> <TD> -0.28 </TD> <TD> 0.44 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 10 using the Youden-Index J threshold is 0.49 (95% CI 0.34 to 0.64). Test specificity is 0.74 (95% CI 0.64 to 0.83). The likelihood ratio of a positive test is 1.9 (95% CI 1.2 to 2.98). The number needed to diagnose (NND) is 4.35 (95% CI 2.13 to -50.00). Around 44 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 5 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 15 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails26.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-IV Alcohol Abuse, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>14. DSM-IV Alcohol Abuse by AUDIT-C Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails27.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Abuse with AUDIT-C score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-IV Alcohol Abuse. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Abuse with AUDIT-C score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 48 </TD> <TD> 73 </TD> <TD> 1 </TD> <TD> 16 </TD> <TD> 0.98 (0.89 to 1.00) </TD> <TD> 0.18 (0.11 to 0.28) </TD> <TD> 0.40 (0.31 to 0.49) </TD> <TD> 0.94 (0.71 to 1.00) </TD> <TD> 1.19 (1.07 to 1.33) </TD> <TD> 0.16 (-0.00 to 0.27) </TD> <TD> 0.56 </TD> <TD> -0.24 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 44 </TD> <TD> 67 </TD> <TD> 5 </TD> <TD> 22 </TD> <TD> 0.90 (0.78 to 0.97) </TD> <TD> 0.25 (0.16 to 0.35) </TD> <TD> 0.40 (0.30 to 0.49) </TD> <TD> 0.81 (0.62 to 0.94) </TD> <TD> 1.19 (1.02 to 1.39) </TD> <TD> 0.15 (-0.06 to 0.32) </TD> <TD> 0.48 </TD> <TD> -0.18 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 41 </TD> <TD> 57 </TD> <TD> 8 </TD> <TD> 32 </TD> <TD> 0.84 (0.70 to 0.93) </TD> <TD> 0.36 (0.26 to 0.47) </TD> <TD> 0.42 (0.32 to 0.52) </TD> <TD> 0.80 (0.64 to 0.91) </TD> <TD> 1.31 (1.07 to 1.59) </TD> <TD> 0.20 (-0.04 to 0.39) </TD> <TD> 0.44 </TD> <TD> -0.04 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 33 </TD> <TD> 44 </TD> <TD> 16 </TD> <TD> 45 </TD> <TD> 0.67 (0.52 to 0.80) </TD> <TD> 0.51 (0.40 to 0.61) </TD> <TD> 0.43 (0.32 to 0.55) </TD> <TD> 0.74 (0.61 to 0.84) </TD> <TD> 1.36 (1.02 to 1.81) </TD> <TD> 0.18 (-0.08 to 0.41) </TD> <TD> 0.26 </TD> <TD> 0.10 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 27 </TD> <TD> 36 </TD> <TD> 22 </TD> <TD> 53 </TD> <TD> 0.55 (0.40 to 0.69) </TD> <TD> 0.60 (0.49 to 0.70) </TD> <TD> 0.43 (0.30 to 0.56) </TD> <TD> 0.71 (0.59 to 0.81) </TD> <TD> 1.36 (0.95 to 1.95) </TD> <TD> 0.15 (-0.11 to 0.39) </TD> <TD> 0.12 </TD> <TD> 0.17 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 17 </TD> <TD> 23 </TD> <TD> 32 </TD> <TD> 66 </TD> <TD> 0.35 (0.22 to 0.50) </TD> <TD> 0.74 (0.64 to 0.83) </TD> <TD> 0.42 (0.27 to 0.59) </TD> <TD> 0.67 (0.57 to 0.76) </TD> <TD> 1.34 (0.80 to 2.26) </TD> <TD> 0.09 (-0.15 to 0.32) </TD> <TD> -0.11 </TD> <TD> 0.28 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 11 </TD> <TD> 15 </TD> <TD> 38 </TD> <TD> 74 </TD> <TD> 0.22 (0.12 to 0.37) </TD> <TD> 0.83 (0.74 to 0.90) </TD> <TD> 0.42 (0.23 to 0.63) </TD> <TD> 0.66 (0.57 to 0.75) </TD> <TD> 1.33 (0.66 to 2.67) </TD> <TD> 0.06 (-0.14 to 0.27) </TD> <TD> -0.26 </TD> <TD> 0.35 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 7 </TD> <TD> 9 </TD> <TD> 42 </TD> <TD> 80 </TD> <TD> 0.14 (0.06 to 0.27) </TD> <TD> 0.90 (0.82 to 0.95) </TD> <TD> 0.44 (0.20 to 0.70) </TD> <TD> 0.66 (0.56 to 0.74) </TD> <TD> 1.41 (0.56 to 3.56) </TD> <TD> 0.04 (-0.12 to 0.23) </TD> <TD> -0.34 </TD> <TD> 0.42 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 4 using the Youden-Index J threshold is 0.84 (95% CI 0.7 to 0.93). Test specificity is 0.36 (95% CI 0.26 to 0.47). The likelihood ratio of a positive test is 1.31 (95% CI 1.07 to 1.59). The number needed to diagnose (NND) is 5.00 (95% CI 2.56 to -25.00). Around 50 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 9 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails28.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-IV Alcohol Abuse, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>15. DSM-IV Alcohol Abuse by AUDIT Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails29.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Abuse with AUDIT score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-IV Alcohol Abuse. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Abuse with AUDIT score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 49 </TD> <TD> 183 </TD> <TD> 4 </TD> <TD> 46 </TD> <TD> 0.92 (0.82 to 0.98) </TD> <TD> 0.20 (0.15 to 0.26) </TD> <TD> 0.21 (0.16 to 0.27) </TD> <TD> 0.92 (0.81 to 0.98) </TD> <TD> 1.16 (1.05 to 1.28) </TD> <TD> 0.13 (-0.03 to 0.24) </TD> <TD> 0.48 </TD> <TD> -0.24 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 46 </TD> <TD> 160 </TD> <TD> 7 </TD> <TD> 69 </TD> <TD> 0.87 (0.75 to 0.95) </TD> <TD> 0.30 (0.24 to 0.37) </TD> <TD> 0.22 (0.17 to 0.29) </TD> <TD> 0.91 (0.82 to 0.96) </TD> <TD> 1.24 (1.09 to 1.42) </TD> <TD> 0.17 (-0.01 to 0.31) </TD> <TD> 0.45 </TD> <TD> -0.11 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 42 </TD> <TD> 130 </TD> <TD> 11 </TD> <TD> 99 </TD> <TD> 0.79 (0.66 to 0.89) </TD> <TD> 0.43 (0.37 to 0.50) </TD> <TD> 0.24 (0.18 to 0.32) </TD> <TD> 0.90 (0.83 to 0.95) </TD> <TD> 1.40 (1.17 to 1.67) </TD> <TD> 0.22 (0.03 to 0.39) </TD> <TD> 0.40 </TD> <TD> 0.04 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 38 </TD> <TD> 100 </TD> <TD> 15 </TD> <TD> 129 </TD> <TD> 0.72 (0.58 to 0.83) </TD> <TD> 0.56 (0.50 to 0.63) </TD> <TD> 0.28 (0.20 to 0.36) </TD> <TD> 0.90 (0.83 to 0.94) </TD> <TD> 1.64 (1.31 to 2.05) </TD> <TD> 0.28 (0.07 to 0.46) </TD> <TD> 0.36 </TD> <TD> 0.20 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 32 </TD> <TD> 80 </TD> <TD> 21 </TD> <TD> 149 </TD> <TD> 0.60 (0.46 to 0.74) </TD> <TD> 0.65 (0.59 to 0.71) </TD> <TD> 0.29 (0.20 to 0.38) </TD> <TD> 0.88 (0.82 to 0.92) </TD> <TD> 1.73 (1.31 to 2.29) </TD> <TD> 0.25 (0.05 to 0.45) </TD> <TD> 0.22 </TD> <TD> 0.28 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 29 </TD> <TD> 66 </TD> <TD> 24 </TD> <TD> 163 </TD> <TD> 0.55 (0.40 to 0.68) </TD> <TD> 0.71 (0.65 to 0.77) </TD> <TD> 0.31 (0.21 to 0.41) </TD> <TD> 0.87 (0.82 to 0.92) </TD> <TD> 1.90 (1.38 to 2.61) </TD> <TD> 0.26 (0.05 to 0.45) </TD> <TD> 0.18 </TD> <TD> 0.34 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 23 </TD> <TD> 52 </TD> <TD> 30 </TD> <TD> 177 </TD> <TD> 0.43 (0.30 to 0.58) </TD> <TD> 0.77 (0.71 to 0.83) </TD> <TD> 0.31 (0.21 to 0.42) </TD> <TD> 0.86 (0.80 to 0.90) </TD> <TD> 1.91 (1.29 to 2.82) </TD> <TD> 0.21 (0.01 to 0.40) </TD> <TD> 0.03 </TD> <TD> 0.37 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 16 </TD> <TD> 42 </TD> <TD> 37 </TD> <TD> 187 </TD> <TD> 0.30 (0.18 to 0.44) </TD> <TD> 0.82 (0.76 to 0.86) </TD> <TD> 0.28 (0.17 to 0.41) </TD> <TD> 0.83 (0.78 to 0.88) </TD> <TD> 1.65 (1.01 to 2.69) </TD> <TD> 0.12 (-0.06 to 0.31) </TD> <TD> -0.14 </TD> <TD> 0.38 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 13 </TD> <TD> 32 </TD> <TD> 40 </TD> <TD> 197 </TD> <TD> 0.25 (0.14 to 0.38) </TD> <TD> 0.86 (0.81 to 0.90) </TD> <TD> 0.29 (0.16 to 0.44) </TD> <TD> 0.83 (0.78 to 0.88) </TD> <TD> 1.76 (0.99 to 3.11) </TD> <TD> 0.11 (-0.05 to 0.29) </TD> <TD> -0.20 </TD> <TD> 0.42 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 5 using the Youden-Index J threshold is 0.72 (95% CI 0.58 to 0.83). Test specificity is 0.56 (95% CI 0.5 to 0.63). The likelihood ratio of a positive test is 1.64 (95% CI 1.31 to 2.05). The number needed to diagnose (NND) is 3.57 (95% CI 2.17 to 14.29). Around 36 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 10 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails30.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-IV Alcohol Abuse, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>16. DSM-IV Alcohol Abuse by AUDIT-C Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails31.png) <br><center><b>Figure: Predicting DSM-IV Alcohol Abuse with AUDIT-C score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-IV Alcohol Abuse. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-IV Alcohol Abuse with AUDIT-C score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 49 </TD> <TD> 182 </TD> <TD> 4 </TD> <TD> 47 </TD> <TD> 0.92 (0.82 to 0.98) </TD> <TD> 0.21 (0.15 to 0.26) </TD> <TD> 0.21 (0.16 to 0.27) </TD> <TD> 0.92 (0.81 to 0.98) </TD> <TD> 1.16 (1.05 to 1.29) </TD> <TD> 0.13 (-0.03 to 0.24) </TD> <TD> 0.49 </TD> <TD> -0.22 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 44 </TD> <TD> 152 </TD> <TD> 9 </TD> <TD> 77 </TD> <TD> 0.83 (0.70 to 0.92) </TD> <TD> 0.34 (0.28 to 0.40) </TD> <TD> 0.22 (0.17 to 0.29) </TD> <TD> 0.90 (0.81 to 0.95) </TD> <TD> 1.25 (1.07 to 1.46) </TD> <TD> 0.17 (-0.02 to 0.32) </TD> <TD> 0.41 </TD> <TD> -0.07 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 35 </TD> <TD> 117 </TD> <TD> 18 </TD> <TD> 112 </TD> <TD> 0.66 (0.52 to 0.78) </TD> <TD> 0.49 (0.42 to 0.56) </TD> <TD> 0.23 (0.17 to 0.31) </TD> <TD> 0.86 (0.79 to 0.92) </TD> <TD> 1.29 (1.03 to 1.63) </TD> <TD> 0.15 (-0.06 to 0.34) </TD> <TD> 0.23 </TD> <TD> 0.06 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 28 </TD> <TD> 82 </TD> <TD> 25 </TD> <TD> 147 </TD> <TD> 0.53 (0.39 to 0.67) </TD> <TD> 0.64 (0.58 to 0.70) </TD> <TD> 0.25 (0.18 to 0.35) </TD> <TD> 0.85 (0.79 to 0.90) </TD> <TD> 1.48 (1.08 to 2.01) </TD> <TD> 0.17 (-0.04 to 0.37) </TD> <TD> 0.11 </TD> <TD> 0.23 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 17 </TD> <TD> 58 </TD> <TD> 36 </TD> <TD> 171 </TD> <TD> 0.32 (0.20 to 0.46) </TD> <TD> 0.75 (0.69 to 0.80) </TD> <TD> 0.23 (0.14 to 0.34) </TD> <TD> 0.83 (0.77 to 0.88) </TD> <TD> 1.27 (0.81 to 1.99) </TD> <TD> 0.07 (-0.12 to 0.26) </TD> <TD> -0.15 </TD> <TD> 0.28 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 10 </TD> <TD> 41 </TD> <TD> 43 </TD> <TD> 188 </TD> <TD> 0.19 (0.09 to 0.32) </TD> <TD> 0.82 (0.77 to 0.87) </TD> <TD> 0.20 (0.10 to 0.33) </TD> <TD> 0.81 (0.76 to 0.86) </TD> <TD> 1.05 (0.57 to 1.97) </TD> <TD> 0.01 (-0.14 to 0.19) </TD> <TD> -0.30 </TD> <TD> 0.32 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 5 </TD> <TD> 19 </TD> <TD> 48 </TD> <TD> 210 </TD> <TD> 0.09 (0.03 to 0.21) </TD> <TD> 0.92 (0.87 to 0.95) </TD> <TD> 0.21 (0.07 to 0.42) </TD> <TD> 0.81 (0.76 to 0.86) </TD> <TD> 1.14 (0.44 to 2.91) </TD> <TD> 0.01 (-0.10 to 0.16) </TD> <TD> -0.41 </TD> <TD> 0.43 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 2 </TD> <TD> 10 </TD> <TD> 51 </TD> <TD> 219 </TD> <TD> 0.04 (0.00 to 0.13) </TD> <TD> 0.96 (0.92 to 0.98) </TD> <TD> 0.17 (0.02 to 0.48) </TD> <TD> 0.81 (0.76 to 0.86) </TD> <TD> 0.86 (0.20 to 3.83) </TD> <TD> -0.01 (-0.07 to 0.11) </TD> <TD> -0.46 </TD> <TD> 0.46 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 0 </TD> <TD> 3 </TD> <TD> 53 </TD> <TD> 226 </TD> <TD> 0.00 (0.00 to 0.10) </TD> <TD> 0.99 (0.96 to 1.00) </TD> <TD> 0.00 (0.00 to 0.81) </TD> <TD> 0.81 (0.76 to 0.85) </TD> <TD> 0.00 (0.00 to NaN) </TD> <TD> -0.01 (-0.04 to 0.10) </TD> <TD> -0.51 </TD> <TD> 0.48 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 5 using the Youden-Index J threshold is 0.53 (95% CI 0.39 to 0.67). Test specificity is 0.64 (95% CI 0.58 to 0.7). The likelihood ratio of a positive test is 1.48 (95% CI 1.08 to 2.01). The number needed to diagnose (NND) is 5.88 (95% CI 2.70 to -25.00). Around 59 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 10 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails32.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-IV Alcohol Abuse, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>17. DSM-5 Alcohol Use Disorder: none vs. mild-severe by AUDIT Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails33.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 4 </TD> <TD> 57 </TD> <TD> 55 </TD> <TD> 8 </TD> <TD> 18 </TD> <TD> 0.88 (0.77 to 0.95) </TD> <TD> 0.25 (0.15 to 0.36) </TD> <TD> 0.51 (0.41 to 0.60) </TD> <TD> 0.69 (0.48 to 0.86) </TD> <TD> 1.16 (0.99 to 1.37) </TD> <TD> 0.12 (-0.08 to 0.31) </TD> <TD> 0.45 </TD> <TD> -0.19 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 50 </TD> <TD> 49 </TD> <TD> 15 </TD> <TD> 24 </TD> <TD> 0.77 (0.65 to 0.86) </TD> <TD> 0.33 (0.22 to 0.45) </TD> <TD> 0.51 (0.40 to 0.61) </TD> <TD> 0.62 (0.45 to 0.77) </TD> <TD> 1.15 (0.93 to 1.41) </TD> <TD> 0.10 (-0.13 to 0.31) </TD> <TD> 0.32 </TD> <TD> -0.12 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 42 </TD> <TD> 44 </TD> <TD> 23 </TD> <TD> 29 </TD> <TD> 0.65 (0.52 to 0.76) </TD> <TD> 0.40 (0.28 to 0.52) </TD> <TD> 0.49 (0.38 to 0.60) </TD> <TD> 0.56 (0.41 to 0.70) </TD> <TD> 1.07 (0.83 to 1.39) </TD> <TD> 0.04 (-0.20 to 0.28) </TD> <TD> 0.18 </TD> <TD> -0.07 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 39 </TD> <TD> 34 </TD> <TD> 26 </TD> <TD> 39 </TD> <TD> 0.60 (0.47 to 0.72) </TD> <TD> 0.53 (0.41 to 0.65) </TD> <TD> 0.53 (0.41 to 0.65) </TD> <TD> 0.60 (0.47 to 0.72) </TD> <TD> 1.29 (0.94 to 1.77) </TD> <TD> 0.13 (-0.12 to 0.37) </TD> <TD> 0.17 </TD> <TD> 0.09 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 37 </TD> <TD> 27 </TD> <TD> 28 </TD> <TD> 46 </TD> <TD> 0.57 (0.44 to 0.69) </TD> <TD> 0.63 (0.51 to 0.74) </TD> <TD> 0.58 (0.45 to 0.70) </TD> <TD> 0.62 (0.50 to 0.73) </TD> <TD> 1.54 (1.07 to 2.22) </TD> <TD> 0.20 (-0.05 to 0.43) </TD> <TD> 0.17 </TD> <TD> 0.23 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 35 </TD> <TD> 21 </TD> <TD> 30 </TD> <TD> 52 </TD> <TD> 0.54 (0.41 to 0.66) </TD> <TD> 0.71 (0.59 to 0.81) </TD> <TD> 0.62 (0.49 to 0.75) </TD> <TD> 0.63 (0.52 to 0.74) </TD> <TD> 1.87 (1.22 to 2.86) </TD> <TD> 0.25 (0.00 to 0.48) </TD> <TD> 0.17 </TD> <TD> 0.33 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 31 </TD> <TD> 16 </TD> <TD> 34 </TD> <TD> 57 </TD> <TD> 0.48 (0.35 to 0.60) </TD> <TD> 0.78 (0.67 to 0.87) </TD> <TD> 0.66 (0.51 to 0.79) </TD> <TD> 0.63 (0.52 to 0.73) </TD> <TD> 2.18 (1.32 to 3.60) </TD> <TD> 0.26 (0.02 to 0.47) </TD> <TD> 0.11 </TD> <TD> 0.41 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 26 </TD> <TD> 13 </TD> <TD> 39 </TD> <TD> 60 </TD> <TD> 0.40 (0.28 to 0.53) </TD> <TD> 0.82 (0.71 to 0.90) </TD> <TD> 0.67 (0.50 to 0.81) </TD> <TD> 0.61 (0.50 to 0.70) </TD> <TD> 2.25 (1.26 to 3.99) </TD> <TD> 0.22 (-0.00 to 0.43) </TD> <TD> 0.01 </TD> <TD> 0.43 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 22 </TD> <TD> 7 </TD> <TD> 43 </TD> <TD> 66 </TD> <TD> 0.34 (0.23 to 0.47) </TD> <TD> 0.90 (0.81 to 0.96) </TD> <TD> 0.76 (0.56 to 0.90) </TD> <TD> 0.61 (0.51 to 0.70) </TD> <TD> 3.53 (1.61 to 7.72) </TD> <TD> 0.24 (0.04 to 0.43) </TD> <TD> -0.04 </TD> <TD> 0.52 </TD> </TR>
  <TR> <TD> 13 </TD> <TD> 19 </TD> <TD> 6 </TD> <TD> 46 </TD> <TD> 67 </TD> <TD> 0.29 (0.19 to 0.42) </TD> <TD> 0.92 (0.83 to 0.97) </TD> <TD> 0.76 (0.55 to 0.91) </TD> <TD> 0.59 (0.50 to 0.68) </TD> <TD> 3.56 (1.51 to 8.36) </TD> <TD> 0.21 (0.02 to 0.39) </TD> <TD> -0.10 </TD> <TD> 0.53 </TD> </TR>
  <TR> <TD> 14 </TD> <TD> 15 </TD> <TD> 5 </TD> <TD> 50 </TD> <TD> 68 </TD> <TD> 0.23 (0.14 to 0.35) </TD> <TD> 0.93 (0.85 to 0.98) </TD> <TD> 0.75 (0.51 to 0.91) </TD> <TD> 0.58 (0.48 to 0.67) </TD> <TD> 3.37 (1.30 to 8.76) </TD> <TD> 0.16 (-0.02 to 0.33) </TD> <TD> -0.19 </TD> <TD> 0.51 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 9 using the Youden-Index J threshold is 0.54 (95% CI 0.41 to 0.66). Test specificity is 0.71 (95% CI 0.59 to 0.81). The likelihood ratio of a positive test is 1.87 (95% CI 1.22 to 2.86). The number needed to diagnose (NND) is 4.00 (95% CI 2.08 to Inf). Around 40 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 4 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 13 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails34.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-5 Alcohol Use Disorder: none vs. mild-severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>18. DSM-5 Alcohol Use Disorder: none vs. mild-severe by AUDIT-C Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails35.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT-C score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT-C score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 61 </TD> <TD> 60 </TD> <TD> 4 </TD> <TD> 13 </TD> <TD> 0.94 (0.85 to 0.98) </TD> <TD> 0.18 (0.10 to 0.29) </TD> <TD> 0.50 (0.41 to 0.60) </TD> <TD> 0.76 (0.50 to 0.93) </TD> <TD> 1.14 (1.01 to 1.29) </TD> <TD> 0.12 (-0.05 to 0.27) </TD> <TD> 0.50 </TD> <TD> -0.26 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 55 </TD> <TD> 56 </TD> <TD> 10 </TD> <TD> 17 </TD> <TD> 0.85 (0.74 to 0.92) </TD> <TD> 0.23 (0.14 to 0.35) </TD> <TD> 0.50 (0.40 to 0.59) </TD> <TD> 0.63 (0.42 to 0.81) </TD> <TD> 1.10 (0.94 to 1.30) </TD> <TD> 0.08 (-0.12 to 0.27) </TD> <TD> 0.39 </TD> <TD> -0.23 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 49 </TD> <TD> 49 </TD> <TD> 16 </TD> <TD> 24 </TD> <TD> 0.75 (0.63 to 0.85) </TD> <TD> 0.33 (0.22 to 0.45) </TD> <TD> 0.50 (0.40 to 0.60) </TD> <TD> 0.60 (0.43 to 0.75) </TD> <TD> 1.12 (0.91 to 1.39) </TD> <TD> 0.08 (-0.15 to 0.30) </TD> <TD> 0.29 </TD> <TD> -0.13 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 40 </TD> <TD> 37 </TD> <TD> 25 </TD> <TD> 36 </TD> <TD> 0.62 (0.49 to 0.73) </TD> <TD> 0.49 (0.37 to 0.61) </TD> <TD> 0.52 (0.40 to 0.63) </TD> <TD> 0.59 (0.46 to 0.71) </TD> <TD> 1.21 (0.90 to 1.63) </TD> <TD> 0.11 (-0.14 to 0.35) </TD> <TD> 0.17 </TD> <TD> 0.04 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 34 </TD> <TD> 29 </TD> <TD> 31 </TD> <TD> 44 </TD> <TD> 0.52 (0.40 to 0.65) </TD> <TD> 0.60 (0.48 to 0.72) </TD> <TD> 0.54 (0.41 to 0.67) </TD> <TD> 0.59 (0.47 to 0.70) </TD> <TD> 1.32 (0.91 to 1.90) </TD> <TD> 0.13 (-0.12 to 0.36) </TD> <TD> 0.08 </TD> <TD> 0.16 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 23 </TD> <TD> 17 </TD> <TD> 42 </TD> <TD> 56 </TD> <TD> 0.35 (0.24 to 0.48) </TD> <TD> 0.77 (0.65 to 0.86) </TD> <TD> 0.57 (0.41 to 0.73) </TD> <TD> 0.57 (0.47 to 0.67) </TD> <TD> 1.52 (0.89 to 2.58) </TD> <TD> 0.12 (-0.11 to 0.34) </TD> <TD> -0.09 </TD> <TD> 0.33 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 17 </TD> <TD> 9 </TD> <TD> 48 </TD> <TD> 64 </TD> <TD> 0.26 (0.16 to 0.39) </TD> <TD> 0.88 (0.78 to 0.94) </TD> <TD> 0.65 (0.44 to 0.83) </TD> <TD> 0.57 (0.47 to 0.66) </TD> <TD> 2.12 (1.02 to 4.43) </TD> <TD> 0.14 (-0.06 to 0.33) </TD> <TD> -0.17 </TD> <TD> 0.45 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 12 </TD> <TD> 4 </TD> <TD> 53 </TD> <TD> 69 </TD> <TD> 0.18 (0.10 to 0.30) </TD> <TD> 0.95 (0.87 to 0.98) </TD> <TD> 0.75 (0.48 to 0.93) </TD> <TD> 0.57 (0.47 to 0.66) </TD> <TD> 3.37 (1.14 to 9.93) </TD> <TD> 0.13 (-0.04 to 0.29) </TD> <TD> -0.26 </TD> <TD> 0.51 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 5 </TD> <TD> 2 </TD> <TD> 60 </TD> <TD> 71 </TD> <TD> 0.08 (0.03 to 0.17) </TD> <TD> 0.97 (0.90 to 1.00) </TD> <TD> 0.71 (0.29 to 0.96) </TD> <TD> 0.54 (0.45 to 0.63) </TD> <TD> 2.81 (0.56 to 13.98) </TD> <TD> 0.05 (-0.07 to 0.17) </TD> <TD> -0.40 </TD> <TD> 0.50 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 1 </TD> <TD> 1 </TD> <TD> 64 </TD> <TD> 72 </TD> <TD> 0.02 (0.00 to 0.08) </TD> <TD> 0.99 (0.93 to 1.00) </TD> <TD> 0.50 (0.01 to 0.99) </TD> <TD> 0.53 (0.44 to 0.62) </TD> <TD> 1.12 (0.07 to 17.60) </TD> <TD> 0.00 (-0.07 to 0.08) </TD> <TD> -0.47 </TD> <TD> 0.49 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 6 using the Youden-Index J threshold is 0.52 (95% CI 0.4 to 0.65). Test specificity is 0.6 (95% CI 0.48 to 0.72). The likelihood ratio of a positive test is 1.32 (95% CI 0.91 to 1.9). The number needed to diagnose (NND) is 7.69 (95% CI 2.78 to -8.33). Around 77 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 9 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails36.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-5 Alcohol Use Disorder: none vs. mild-severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>19. DSM-5 Alcohol Use Disorder: none vs. mild-severe by AUDIT Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails37.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 101 </TD> <TD> 131 </TD> <TD> 4 </TD> <TD> 46 </TD> <TD> 0.96 (0.91 to 0.99) </TD> <TD> 0.26 (0.20 to 0.33) </TD> <TD> 0.44 (0.37 to 0.50) </TD> <TD> 0.92 (0.81 to 0.98) </TD> <TD> 1.30 (1.18 to 1.43) </TD> <TD> 0.22 (0.10 to 0.32) </TD> <TD> 0.57 </TD> <TD> -0.13 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 95 </TD> <TD> 111 </TD> <TD> 10 </TD> <TD> 66 </TD> <TD> 0.90 (0.83 to 0.95) </TD> <TD> 0.37 (0.30 to 0.45) </TD> <TD> 0.46 (0.39 to 0.53) </TD> <TD> 0.87 (0.77 to 0.94) </TD> <TD> 1.44 (1.27 to 1.64) </TD> <TD> 0.28 (0.13 to 0.40) </TD> <TD> 0.54 </TD> <TD> 0.00 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 87 </TD> <TD> 85 </TD> <TD> 18 </TD> <TD> 92 </TD> <TD> 0.83 (0.74 to 0.90) </TD> <TD> 0.52 (0.44 to 0.60) </TD> <TD> 0.51 (0.43 to 0.58) </TD> <TD> 0.84 (0.75 to 0.90) </TD> <TD> 1.73 (1.45 to 2.06) </TD> <TD> 0.35 (0.19 to 0.49) </TD> <TD> 0.50 </TD> <TD> 0.20 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 75 </TD> <TD> 63 </TD> <TD> 30 </TD> <TD> 114 </TD> <TD> 0.71 (0.62 to 0.80) </TD> <TD> 0.64 (0.57 to 0.71) </TD> <TD> 0.54 (0.46 to 0.63) </TD> <TD> 0.79 (0.72 to 0.85) </TD> <TD> 2.01 (1.59 to 2.53) </TD> <TD> 0.36 (0.19 to 0.51) </TD> <TD> 0.39 </TD> <TD> 0.31 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 66 </TD> <TD> 46 </TD> <TD> 39 </TD> <TD> 131 </TD> <TD> 0.63 (0.53 to 0.72) </TD> <TD> 0.74 (0.67 to 0.80) </TD> <TD> 0.59 (0.49 to 0.68) </TD> <TD> 0.77 (0.70 to 0.83) </TD> <TD> 2.42 (1.81 to 3.23) </TD> <TD> 0.37 (0.20 to 0.52) </TD> <TD> 0.31 </TD> <TD> 0.42 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 58 </TD> <TD> 37 </TD> <TD> 47 </TD> <TD> 140 </TD> <TD> 0.55 (0.45 to 0.65) </TD> <TD> 0.79 (0.72 to 0.85) </TD> <TD> 0.61 (0.51 to 0.71) </TD> <TD> 0.75 (0.68 to 0.81) </TD> <TD> 2.64 (1.89 to 3.69) </TD> <TD> 0.34 (0.18 to 0.50) </TD> <TD> 0.22 </TD> <TD> 0.46 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 45 </TD> <TD> 30 </TD> <TD> 60 </TD> <TD> 147 </TD> <TD> 0.43 (0.33 to 0.53) </TD> <TD> 0.83 (0.77 to 0.88) </TD> <TD> 0.60 (0.48 to 0.71) </TD> <TD> 0.71 (0.64 to 0.77) </TD> <TD> 2.53 (1.71 to 3.75) </TD> <TD> 0.26 (0.10 to 0.41) </TD> <TD> 0.06 </TD> <TD> 0.46 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 35 </TD> <TD> 23 </TD> <TD> 70 </TD> <TD> 154 </TD> <TD> 0.33 (0.24 to 0.43) </TD> <TD> 0.87 (0.81 to 0.92) </TD> <TD> 0.60 (0.47 to 0.73) </TD> <TD> 0.69 (0.62 to 0.75) </TD> <TD> 2.57 (1.61 to 4.09) </TD> <TD> 0.20 (0.06 to 0.35) </TD> <TD> -0.07 </TD> <TD> 0.47 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 28 </TD> <TD> 17 </TD> <TD> 77 </TD> <TD> 160 </TD> <TD> 0.27 (0.19 to 0.36) </TD> <TD> 0.90 (0.85 to 0.94) </TD> <TD> 0.62 (0.47 to 0.76) </TD> <TD> 0.68 (0.61 to 0.73) </TD> <TD> 2.78 (1.60 to 4.82) </TD> <TD> 0.17 (0.04 to 0.30) </TD> <TD> -0.15 </TD> <TD> 0.49 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 24 </TD> <TD> 13 </TD> <TD> 81 </TD> <TD> 164 </TD> <TD> 0.23 (0.15 to 0.32) </TD> <TD> 0.93 (0.88 to 0.96) </TD> <TD> 0.65 (0.47 to 0.80) </TD> <TD> 0.67 (0.61 to 0.73) </TD> <TD> 3.11 (1.66 to 5.85) </TD> <TD> 0.16 (0.03 to 0.28) </TD> <TD> -0.19 </TD> <TD> 0.51 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 6 using the Youden-Index J threshold is 0.63 (95% CI 0.53 to 0.72). Test specificity is 0.74 (95% CI 0.67 to 0.8). The likelihood ratio of a positive test is 2.42 (95% CI 1.81 to 3.23). The number needed to diagnose (NND) is 2.70 (95% CI 1.92 to 5.00). Around 28 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 11 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails38.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-5 Alcohol Use Disorder: none vs. mild-severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>20. DSM-5 Alcohol Use Disorder: none vs. mild-severe by AUDIT-C Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails39.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT-C score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none vs. mild-severe with AUDIT-C score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 100 </TD> <TD> 131 </TD> <TD> 5 </TD> <TD> 46 </TD> <TD> 0.95 (0.89 to 0.98) </TD> <TD> 0.26 (0.20 to 0.33) </TD> <TD> 0.43 (0.37 to 0.50) </TD> <TD> 0.90 (0.79 to 0.97) </TD> <TD> 1.29 (1.17 to 1.42) </TD> <TD> 0.21 (0.09 to 0.32) </TD> <TD> 0.55 </TD> <TD> -0.14 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 90 </TD> <TD> 106 </TD> <TD> 15 </TD> <TD> 71 </TD> <TD> 0.86 (0.78 to 0.92) </TD> <TD> 0.40 (0.33 to 0.48) </TD> <TD> 0.46 (0.39 to 0.53) </TD> <TD> 0.83 (0.73 to 0.90) </TD> <TD> 1.43 (1.24 to 1.65) </TD> <TD> 0.26 (0.10 to 0.40) </TD> <TD> 0.49 </TD> <TD> 0.03 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 76 </TD> <TD> 76 </TD> <TD> 29 </TD> <TD> 101 </TD> <TD> 0.72 (0.63 to 0.81) </TD> <TD> 0.57 (0.49 to 0.64) </TD> <TD> 0.50 (0.42 to 0.58) </TD> <TD> 0.78 (0.70 to 0.85) </TD> <TD> 1.69 (1.37 to 2.07) </TD> <TD> 0.29 (0.12 to 0.45) </TD> <TD> 0.36 </TD> <TD> 0.21 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 59 </TD> <TD> 51 </TD> <TD> 46 </TD> <TD> 126 </TD> <TD> 0.56 (0.46 to 0.66) </TD> <TD> 0.71 (0.64 to 0.78) </TD> <TD> 0.54 (0.44 to 0.63) </TD> <TD> 0.73 (0.66 to 0.80) </TD> <TD> 1.95 (1.46 to 2.60) </TD> <TD> 0.27 (0.10 to 0.44) </TD> <TD> 0.20 </TD> <TD> 0.34 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 40 </TD> <TD> 35 </TD> <TD> 65 </TD> <TD> 142 </TD> <TD> 0.38 (0.29 to 0.48) </TD> <TD> 0.80 (0.74 to 0.86) </TD> <TD> 0.53 (0.41 to 0.65) </TD> <TD> 0.69 (0.62 to 0.75) </TD> <TD> 1.93 (1.31 to 2.83) </TD> <TD> 0.18 (0.02 to 0.34) </TD> <TD> -0.03 </TD> <TD> 0.39 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 26 </TD> <TD> 25 </TD> <TD> 79 </TD> <TD> 152 </TD> <TD> 0.25 (0.17 to 0.34) </TD> <TD> 0.86 (0.80 to 0.91) </TD> <TD> 0.51 (0.37 to 0.65) </TD> <TD> 0.66 (0.59 to 0.72) </TD> <TD> 1.75 (1.07 to 2.87) </TD> <TD> 0.11 (-0.03 to 0.25) </TD> <TD> -0.20 </TD> <TD> 0.42 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 12 </TD> <TD> 12 </TD> <TD> 93 </TD> <TD> 165 </TD> <TD> 0.11 (0.06 to 0.19) </TD> <TD> 0.93 (0.88 to 0.96) </TD> <TD> 0.50 (0.29 to 0.71) </TD> <TD> 0.64 (0.58 to 0.70) </TD> <TD> 1.69 (0.79 to 3.61) </TD> <TD> 0.05 (-0.05 to 0.16) </TD> <TD> -0.37 </TD> <TD> 0.45 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 6 </TD> <TD> 6 </TD> <TD> 99 </TD> <TD> 171 </TD> <TD> 0.06 (0.02 to 0.12) </TD> <TD> 0.97 (0.93 to 0.99) </TD> <TD> 0.50 (0.21 to 0.79) </TD> <TD> 0.63 (0.57 to 0.69) </TD> <TD> 1.69 (0.56 to 5.09) </TD> <TD> 0.02 (-0.05 to 0.11) </TD> <TD> -0.43 </TD> <TD> 0.49 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 4 using the Youden-Index J threshold is 0.72 (95% CI 0.63 to 0.81). Test specificity is 0.57 (95% CI 0.49 to 0.64). The likelihood ratio of a positive test is 1.69 (95% CI 1.37 to 2.07). The number needed to diagnose (NND) is 3.45 (95% CI 2.22 to 8.33). Around 35 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 9 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails40.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-5 Alcohol Use Disorder: none vs. mild-severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>21. DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe by AUDIT Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails41.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 4 </TD> <TD> 22 </TD> <TD> 90 </TD> <TD> 2 </TD> <TD> 24 </TD> <TD> 0.92 (0.73 to 0.99) </TD> <TD> 0.21 (0.14 to 0.30) </TD> <TD> 0.20 (0.13 to 0.28) </TD> <TD> 0.92 (0.75 to 0.99) </TD> <TD> 1.16 (1.00 to 1.35) </TD> <TD> 0.13 (-0.13 to 0.29) </TD> <TD> 0.49 </TD> <TD> -0.22 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 20 </TD> <TD> 79 </TD> <TD> 4 </TD> <TD> 35 </TD> <TD> 0.83 (0.63 to 0.95) </TD> <TD> 0.31 (0.22 to 0.40) </TD> <TD> 0.20 (0.13 to 0.29) </TD> <TD> 0.90 (0.76 to 0.97) </TD> <TD> 1.20 (0.97 to 1.49) </TD> <TD> 0.14 (-0.15 to 0.35) </TD> <TD> 0.40 </TD> <TD> -0.12 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 18 </TD> <TD> 68 </TD> <TD> 6 </TD> <TD> 46 </TD> <TD> 0.75 (0.53 to 0.90) </TD> <TD> 0.40 (0.31 to 0.50) </TD> <TD> 0.21 (0.13 to 0.31) </TD> <TD> 0.88 (0.77 to 0.96) </TD> <TD> 1.26 (0.95 to 1.66) </TD> <TD> 0.15 (-0.15 to 0.40) </TD> <TD> 0.32 </TD> <TD> -0.02 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 16 </TD> <TD> 57 </TD> <TD> 8 </TD> <TD> 57 </TD> <TD> 0.67 (0.45 to 0.84) </TD> <TD> 0.50 (0.40 to 0.60) </TD> <TD> 0.22 (0.13 to 0.33) </TD> <TD> 0.88 (0.77 to 0.95) </TD> <TD> 1.33 (0.95 to 1.87) </TD> <TD> 0.17 (-0.15 to 0.44) </TD> <TD> 0.26 </TD> <TD> 0.08 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 16 </TD> <TD> 48 </TD> <TD> 8 </TD> <TD> 66 </TD> <TD> 0.67 (0.45 to 0.84) </TD> <TD> 0.58 (0.48 to 0.67) </TD> <TD> 0.25 (0.15 to 0.37) </TD> <TD> 0.89 (0.80 to 0.95) </TD> <TD> 1.58 (1.11 to 2.26) </TD> <TD> 0.25 (-0.07 to 0.51) </TD> <TD> 0.30 </TD> <TD> 0.20 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 16 </TD> <TD> 40 </TD> <TD> 8 </TD> <TD> 74 </TD> <TD> 0.67 (0.45 to 0.84) </TD> <TD> 0.65 (0.55 to 0.74) </TD> <TD> 0.29 (0.17 to 0.42) </TD> <TD> 0.90 (0.82 to 0.96) </TD> <TD> 1.90 (1.30 to 2.77) </TD> <TD> 0.32 (0.00 to 0.58) </TD> <TD> 0.33 </TD> <TD> 0.31 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 14 </TD> <TD> 33 </TD> <TD> 10 </TD> <TD> 81 </TD> <TD> 0.58 (0.37 to 0.78) </TD> <TD> 0.71 (0.62 to 0.79) </TD> <TD> 0.30 (0.17 to 0.45) </TD> <TD> 0.89 (0.81 to 0.95) </TD> <TD> 2.02 (1.29 to 3.14) </TD> <TD> 0.29 (-0.02 to 0.57) </TD> <TD> 0.22 </TD> <TD> 0.35 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 11 </TD> <TD> 28 </TD> <TD> 13 </TD> <TD> 86 </TD> <TD> 0.46 (0.26 to 0.67) </TD> <TD> 0.75 (0.66 to 0.83) </TD> <TD> 0.28 (0.15 to 0.45) </TD> <TD> 0.87 (0.79 to 0.93) </TD> <TD> 1.87 (1.09 to 3.21) </TD> <TD> 0.21 (-0.08 to 0.50) </TD> <TD> 0.06 </TD> <TD> 0.35 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 11 </TD> <TD> 18 </TD> <TD> 13 </TD> <TD> 96 </TD> <TD> 0.46 (0.26 to 0.67) </TD> <TD> 0.84 (0.76 to 0.90) </TD> <TD> 0.38 (0.21 to 0.58) </TD> <TD> 0.88 (0.80 to 0.93) </TD> <TD> 2.90 (1.58 to 5.33) </TD> <TD> 0.30 (0.02 to 0.58) </TD> <TD> 0.11 </TD> <TD> 0.49 </TD> </TR>
  <TR> <TD> 13 </TD> <TD> 10 </TD> <TD> 15 </TD> <TD> 14 </TD> <TD> 99 </TD> <TD> 0.42 (0.22 to 0.63) </TD> <TD> 0.87 (0.79 to 0.92) </TD> <TD> 0.40 (0.21 to 0.61) </TD> <TD> 0.88 (0.80 to 0.93) </TD> <TD> 3.17 (1.62 to 6.18) </TD> <TD> 0.29 (0.01 to 0.56) </TD> <TD> 0.06 </TD> <TD> 0.51 </TD> </TR>
  <TR> <TD> 14 </TD> <TD> 8 </TD> <TD> 12 </TD> <TD> 16 </TD> <TD> 102 </TD> <TD> 0.33 (0.16 to 0.55) </TD> <TD> 0.89 (0.82 to 0.94) </TD> <TD> 0.40 (0.19 to 0.64) </TD> <TD> 0.86 (0.79 to 0.92) </TD> <TD> 3.17 (1.45 to 6.90) </TD> <TD> 0.23 (-0.02 to 0.50) </TD> <TD> -0.06 </TD> <TD> 0.50 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 9 using the Youden-Index J threshold is 0.67 (95% CI 0.45 to 0.84). Test specificity is 0.65 (95% CI 0.55 to 0.74). The likelihood ratio of a positive test is 1.9 (95% CI 1.3 to 2.77). The number needed to diagnose (NND) is 3.12 (95% CI 1.72 to Inf). Around 32 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 4 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 13 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails42.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>22. DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe by AUDIT-C Score: Males aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails43.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT-C score (Males aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT-C score (Males aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 21 </TD> <TD> 100 </TD> <TD> 3 </TD> <TD> 14 </TD> <TD> 0.88 (0.68 to 0.97) </TD> <TD> 0.12 (0.07 to 0.20) </TD> <TD> 0.17 (0.11 to 0.25) </TD> <TD> 0.82 (0.57 to 0.96) </TD> <TD> 1.00 (0.84 to 1.18) </TD> <TD> -0.00 (-0.25 to 0.17) </TD> <TD> 0.38 </TD> <TD> -0.38 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 20 </TD> <TD> 91 </TD> <TD> 4 </TD> <TD> 23 </TD> <TD> 0.83 (0.63 to 0.95) </TD> <TD> 0.20 (0.13 to 0.29) </TD> <TD> 0.18 (0.11 to 0.26) </TD> <TD> 0.85 (0.66 to 0.96) </TD> <TD> 1.04 (0.85 to 1.28) </TD> <TD> 0.04 (-0.24 to 0.24) </TD> <TD> 0.34 </TD> <TD> -0.28 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 18 </TD> <TD> 80 </TD> <TD> 6 </TD> <TD> 34 </TD> <TD> 0.75 (0.53 to 0.90) </TD> <TD> 0.30 (0.22 to 0.39) </TD> <TD> 0.18 (0.11 to 0.27) </TD> <TD> 0.85 (0.70 to 0.94) </TD> <TD> 1.07 (0.82 to 1.39) </TD> <TD> 0.05 (-0.25 to 0.29) </TD> <TD> 0.27 </TD> <TD> -0.18 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 16 </TD> <TD> 61 </TD> <TD> 8 </TD> <TD> 53 </TD> <TD> 0.67 (0.45 to 0.84) </TD> <TD> 0.46 (0.37 to 0.56) </TD> <TD> 0.21 (0.12 to 0.32) </TD> <TD> 0.87 (0.76 to 0.94) </TD> <TD> 1.25 (0.90 to 1.73) </TD> <TD> 0.13 (-0.18 to 0.40) </TD> <TD> 0.24 </TD> <TD> 0.03 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 14 </TD> <TD> 49 </TD> <TD> 10 </TD> <TD> 65 </TD> <TD> 0.58 (0.37 to 0.78) </TD> <TD> 0.57 (0.47 to 0.66) </TD> <TD> 0.22 (0.13 to 0.34) </TD> <TD> 0.87 (0.77 to 0.93) </TD> <TD> 1.36 (0.91 to 2.02) </TD> <TD> 0.15 (-0.16 to 0.44) </TD> <TD> 0.15 </TD> <TD> 0.15 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 9 </TD> <TD> 31 </TD> <TD> 15 </TD> <TD> 83 </TD> <TD> 0.38 (0.19 to 0.59) </TD> <TD> 0.73 (0.64 to 0.81) </TD> <TD> 0.23 (0.11 to 0.38) </TD> <TD> 0.85 (0.76 to 0.91) </TD> <TD> 1.38 (0.76 to 2.51) </TD> <TD> 0.10 (-0.18 to 0.40) </TD> <TD> -0.06 </TD> <TD> 0.28 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 6 </TD> <TD> 20 </TD> <TD> 18 </TD> <TD> 94 </TD> <TD> 0.25 (0.10 to 0.47) </TD> <TD> 0.82 (0.74 to 0.89) </TD> <TD> 0.23 (0.09 to 0.44) </TD> <TD> 0.84 (0.76 to 0.90) </TD> <TD> 1.43 (0.64 to 3.17) </TD> <TD> 0.07 (-0.16 to 0.36) </TD> <TD> -0.22 </TD> <TD> 0.35 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 3 </TD> <TD> 13 </TD> <TD> 21 </TD> <TD> 101 </TD> <TD> 0.12 (0.03 to 0.32) </TD> <TD> 0.89 (0.81 to 0.94) </TD> <TD> 0.19 (0.04 to 0.46) </TD> <TD> 0.83 (0.75 to 0.89) </TD> <TD> 1.10 (0.34 to 3.55) </TD> <TD> 0.01 (-0.16 to 0.26) </TD> <TD> -0.38 </TD> <TD> 0.40 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 2 </TD> <TD> 5 </TD> <TD> 22 </TD> <TD> 109 </TD> <TD> 0.08 (0.01 to 0.27) </TD> <TD> 0.96 (0.90 to 0.99) </TD> <TD> 0.29 (0.04 to 0.71) </TD> <TD> 0.83 (0.76 to 0.89) </TD> <TD> 1.90 (0.39 to 9.22) </TD> <TD> 0.04 (-0.09 to 0.26) </TD> <TD> -0.40 </TD> <TD> 0.48 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 1 </TD> <TD> 1 </TD> <TD> 23 </TD> <TD> 113 </TD> <TD> 0.04 (0.00 to 0.21) </TD> <TD> 0.99 (0.95 to 1.00) </TD> <TD> 0.50 (0.01 to 0.99) </TD> <TD> 0.83 (0.76 to 0.89) </TD> <TD> 4.75 (0.31 to 73.32) </TD> <TD> 0.03 (-0.05 to 0.21) </TD> <TD> -0.45 </TD> <TD> 0.50 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 6 using the Youden-Index J threshold is 0.58 (95% CI 0.37 to 0.78). Test specificity is 0.57 (95% CI 0.47 to 0.66). The likelihood ratio of a positive test is 1.36 (95% CI 0.91 to 2.02). The number needed to diagnose (NND) is 6.67 (95% CI 2.27 to -6.25). Around 67 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 11 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails44.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>23. DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe by AUDIT Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails45.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT in predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 3 </TD> <TD> 22 </TD> <TD> 184 </TD> <TD> 2 </TD> <TD> 74 </TD> <TD> 0.92 (0.73 to 0.99) </TD> <TD> 0.29 (0.23 to 0.35) </TD> <TD> 0.11 (0.07 to 0.16) </TD> <TD> 0.97 (0.91 to 1.00) </TD> <TD> 1.29 (1.11 to 1.48) </TD> <TD> 0.20 (-0.04 to 0.34) </TD> <TD> 0.53 </TD> <TD> -0.10 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 21 </TD> <TD> 151 </TD> <TD> 3 </TD> <TD> 107 </TD> <TD> 0.88 (0.68 to 0.97) </TD> <TD> 0.41 (0.35 to 0.48) </TD> <TD> 0.12 (0.08 to 0.18) </TD> <TD> 0.97 (0.92 to 0.99) </TD> <TD> 1.50 (1.25 to 1.79) </TD> <TD> 0.29 (0.03 to 0.45) </TD> <TD> 0.53 </TD> <TD> 0.05 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 18 </TD> <TD> 120 </TD> <TD> 6 </TD> <TD> 138 </TD> <TD> 0.75 (0.53 to 0.90) </TD> <TD> 0.53 (0.47 to 0.60) </TD> <TD> 0.13 (0.08 to 0.20) </TD> <TD> 0.96 (0.91 to 0.98) </TD> <TD> 1.61 (1.24 to 2.10) </TD> <TD> 0.28 (0.00 to 0.50) </TD> <TD> 0.39 </TD> <TD> 0.17 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 18 </TD> <TD> 94 </TD> <TD> 6 </TD> <TD> 164 </TD> <TD> 0.75 (0.53 to 0.90) </TD> <TD> 0.64 (0.57 to 0.69) </TD> <TD> 0.16 (0.10 to 0.24) </TD> <TD> 0.96 (0.92 to 0.99) </TD> <TD> 2.06 (1.55 to 2.73) </TD> <TD> 0.39 (0.11 to 0.60) </TD> <TD> 0.45 </TD> <TD> 0.33 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 17 </TD> <TD> 78 </TD> <TD> 7 </TD> <TD> 180 </TD> <TD> 0.71 (0.49 to 0.87) </TD> <TD> 0.70 (0.64 to 0.75) </TD> <TD> 0.18 (0.11 to 0.27) </TD> <TD> 0.96 (0.92 to 0.98) </TD> <TD> 2.34 (1.71 to 3.22) </TD> <TD> 0.41 (0.13 to 0.63) </TD> <TD> 0.42 </TD> <TD> 0.40 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 16 </TD> <TD> 59 </TD> <TD> 8 </TD> <TD> 199 </TD> <TD> 0.67 (0.45 to 0.84) </TD> <TD> 0.77 (0.72 to 0.82) </TD> <TD> 0.21 (0.13 to 0.32) </TD> <TD> 0.96 (0.93 to 0.98) </TD> <TD> 2.92 (2.03 to 4.18) </TD> <TD> 0.44 (0.16 to 0.66) </TD> <TD> 0.39 </TD> <TD> 0.49 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 11 </TD> <TD> 47 </TD> <TD> 13 </TD> <TD> 211 </TD> <TD> 0.46 (0.26 to 0.67) </TD> <TD> 0.82 (0.77 to 0.86) </TD> <TD> 0.19 (0.10 to 0.31) </TD> <TD> 0.94 (0.90 to 0.97) </TD> <TD> 2.52 (1.52 to 4.17) </TD> <TD> 0.28 (0.02 to 0.53) </TD> <TD> 0.10 </TD> <TD> 0.46 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 9 </TD> <TD> 36 </TD> <TD> 15 </TD> <TD> 222 </TD> <TD> 0.38 (0.19 to 0.59) </TD> <TD> 0.86 (0.81 to 0.90) </TD> <TD> 0.20 (0.10 to 0.35) </TD> <TD> 0.94 (0.90 to 0.96) </TD> <TD> 2.69 (1.48 to 4.89) </TD> <TD> 0.24 (0.00 to 0.49) </TD> <TD> 0.00 </TD> <TD> 0.48 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 9 </TD> <TD> 28 </TD> <TD> 15 </TD> <TD> 230 </TD> <TD> 0.38 (0.19 to 0.59) </TD> <TD> 0.89 (0.85 to 0.93) </TD> <TD> 0.24 (0.12 to 0.41) </TD> <TD> 0.94 (0.90 to 0.97) </TD> <TD> 3.46 (1.85 to 6.45) </TD> <TD> 0.27 (0.03 to 0.52) </TD> <TD> 0.02 </TD> <TD> 0.52 </TD> </TR>
  <TR> <TD> 12 </TD> <TD> 7 </TD> <TD> 19 </TD> <TD> 17 </TD> <TD> 239 </TD> <TD> 0.29 (0.13 to 0.51) </TD> <TD> 0.93 (0.89 to 0.96) </TD> <TD> 0.27 (0.12 to 0.48) </TD> <TD> 0.93 (0.90 to 0.96) </TD> <TD> 3.96 (1.85 to 8.46) </TD> <TD> 0.22 (0.01 to 0.47) </TD> <TD> -0.10 </TD> <TD> 0.54 </TD> </TR>
  <TR> <TD> 13 </TD> <TD> 5 </TD> <TD> 14 </TD> <TD> 19 </TD> <TD> 244 </TD> <TD> 0.21 (0.07 to 0.42) </TD> <TD> 0.95 (0.91 to 0.97) </TD> <TD> 0.26 (0.09 to 0.51) </TD> <TD> 0.93 (0.89 to 0.96) </TD> <TD> 3.84 (1.51 to 9.75) </TD> <TD> 0.15 (-0.02 to 0.39) </TD> <TD> -0.21 </TD> <TD> 0.53 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 8 using the Youden-Index J threshold is 0.67 (95% CI 0.45 to 0.84). Test specificity is 0.77 (95% CI 0.72 to 0.82). The likelihood ratio of a positive test is 2.92 (95% CI 2.03 to 4.18). The number needed to diagnose (NND) is 2.27 (95% CI 1.52 to 6.25). Around 23 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 3 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 12 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails46.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT, of DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br><h4><u>24. DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe by AUDIT-C Score: Females aged 18-35</u></h4><p>![plot of chunk showdetails](figure/showdetails47.png) <br><center><b>Figure: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT-C score (Females aged 18-35): ROC Plot with 95% Confidence Region</b></center><br><br>This plot shows the overall accuracy of AUDIT-C in predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe. The Receiver Operating Characteristic (ROC) curve is shown in red (dotted line) with a smoothed version in blue. A 95% confidence interval band for the smoothed ROC is also shown. The area under the curve (AUC) is shown with a 95% Confidence Interval. One approach to selecting an optimal cut-point for a test score is to use the point on the ROC curve which is closest to the upper left corner of the plot. An alternative approach is the use the Youden J index, which represents the point on the ROC curve which is furthest away from the diagonal 45º line that indicates an uninformative test. The Youden J index is used in the study and is shown in the Table below along with a discussion of alternative weightings and interpretations of J.<br><br><br><TABLE border=1>
<CAPTION ALIGN="bottom"> <br><b>Table: Predicting DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe with AUDIT-C score (Females aged 18-35):<br>Summary Statistics, 95% Confidence Intervals and Weighted Thresholds</b><br> </CAPTION>
<TR> <TH> Test Score (TS) </TH> <TH> TP </TH> <TH> FP </TH> <TH> FN </TH> <TH> TN </TH> <TH> Sens (95% CI) </TH> <TH> Spec (95% CI) </TH> <TH> PPV (95% CI) </TH> <TH> NPV (95% CI) </TH> <TH> +LR (95% CI) </TH> <TH> Youden J (95% CI) </TH> <TH> TS: Jw for Sens </TH> <TH> TS: Jw for Spec </TH>  </TR>
  <TR> <TD> 2 </TD> <TD> 23 </TD> <TD> 208 </TD> <TD> 1 </TD> <TD> 50 </TD> <TD> 0.96 (0.79 to 1.00) </TD> <TD> 0.19 (0.15 to 0.25) </TD> <TD> 0.10 (0.06 to 0.15) </TD> <TD> 0.98 (0.90 to 1.00) </TD> <TD> 1.19 (1.07 to 1.32) </TD> <TD> 0.15 (-0.06 to 0.25) </TD> <TD> 0.53 </TD> <TD> -0.23 </TD> </TR>
  <TR> <TD> 3 </TD> <TD> 20 </TD> <TD> 176 </TD> <TD> 4 </TD> <TD> 82 </TD> <TD> 0.83 (0.63 to 0.95) </TD> <TD> 0.32 (0.26 to 0.38) </TD> <TD> 0.10 (0.06 to 0.15) </TD> <TD> 0.95 (0.89 to 0.99) </TD> <TD> 1.22 (1.00 to 1.49) </TD> <TD> 0.15 (-0.11 to 0.33) </TD> <TD> 0.40 </TD> <TD> -0.10 </TD> </TR>
  <TR> <TD> 4 </TD> <TD> 18 </TD> <TD> 134 </TD> <TD> 6 </TD> <TD> 124 </TD> <TD> 0.75 (0.53 to 0.90) </TD> <TD> 0.48 (0.42 to 0.54) </TD> <TD> 0.12 (0.07 to 0.18) </TD> <TD> 0.95 (0.90 to 0.98) </TD> <TD> 1.44 (1.11 to 1.87) </TD> <TD> 0.23 (-0.05 to 0.45) </TD> <TD> 0.36 </TD> <TD> 0.09 </TD> </TR>
  <TR> <TD> 5 </TD> <TD> 15 </TD> <TD> 95 </TD> <TD> 9 </TD> <TD> 163 </TD> <TD> 0.62 (0.41 to 0.81) </TD> <TD> 0.63 (0.57 to 0.69) </TD> <TD> 0.14 (0.08 to 0.21) </TD> <TD> 0.95 (0.90 to 0.98) </TD> <TD> 1.70 (1.20 to 2.41) </TD> <TD> 0.26 (-0.02 to 0.50) </TD> <TD> 0.24 </TD> <TD> 0.26 </TD> </TR>
  <TR> <TD> 6 </TD> <TD> 13 </TD> <TD> 62 </TD> <TD> 11 </TD> <TD> 196 </TD> <TD> 0.54 (0.33 to 0.74) </TD> <TD> 0.76 (0.70 to 0.81) </TD> <TD> 0.17 (0.10 to 0.28) </TD> <TD> 0.95 (0.91 to 0.97) </TD> <TD> 2.25 (1.47 to 3.46) </TD> <TD> 0.30 (0.03 to 0.55) </TD> <TD> 0.19 </TD> <TD> 0.41 </TD> </TR>
  <TR> <TD> 7 </TD> <TD> 9 </TD> <TD> 42 </TD> <TD> 15 </TD> <TD> 216 </TD> <TD> 0.38 (0.19 to 0.59) </TD> <TD> 0.84 (0.79 to 0.88) </TD> <TD> 0.18 (0.08 to 0.31) </TD> <TD> 0.94 (0.90 to 0.96) </TD> <TD> 2.30 (1.28 to 4.14) </TD> <TD> 0.21 (-0.03 to 0.47) </TD> <TD> -0.01 </TD> <TD> 0.45 </TD> </TR>
  <TR> <TD> 8 </TD> <TD> 4 </TD> <TD> 20 </TD> <TD> 20 </TD> <TD> 238 </TD> <TD> 0.17 (0.05 to 0.37) </TD> <TD> 0.92 (0.88 to 0.95) </TD> <TD> 0.17 (0.05 to 0.37) </TD> <TD> 0.92 (0.88 to 0.95) </TD> <TD> 2.15 (0.80 to 5.78) </TD> <TD> 0.09 (-0.07 to 0.33) </TD> <TD> -0.28 </TD> <TD> 0.47 </TD> </TR>
  <TR> <TD> 9 </TD> <TD> 2 </TD> <TD> 10 </TD> <TD> 22 </TD> <TD> 248 </TD> <TD> 0.08 (0.01 to 0.27) </TD> <TD> 0.96 (0.93 to 0.98) </TD> <TD> 0.17 (0.02 to 0.48) </TD> <TD> 0.92 (0.88 to 0.95) </TD> <TD> 2.15 (0.50 to 9.25) </TD> <TD> 0.04 (-0.06 to 0.25) </TD> <TD> -0.40 </TD> <TD> 0.48 </TD> </TR>
  <TR> <TD> 10 </TD> <TD> 1 </TD> <TD> 2 </TD> <TD> 23 </TD> <TD> 256 </TD> <TD> 0.04 (0.00 to 0.21) </TD> <TD> 0.99 (0.97 to 1.00) </TD> <TD> 0.33 (0.01 to 0.91) </TD> <TD> 0.92 (0.88 to 0.95) </TD> <TD> 5.37 (0.51 to 57.14) </TD> <TD> 0.03 (-0.03 to 0.21) </TD> <TD> -0.45 </TD> <TD> 0.50 </TD> </TR>
  <TR> <TD> 11 </TD> <TD> 0 </TD> <TD> 0 </TD> <TD> 24 </TD> <TD> 258 </TD> <TD> 0.00 (0.00 to 0.20) </TD> <TD> 1.00 (0.98 to 1.00) </TD> <TD> NaN (0.00 to 1.00) </TD> <TD> 0.91 (0.88 to 0.94) </TD> <TD> NaN (NaN to NaN) </TD> <TD> 0.00 (-0.02 to 0.20) </TD> <TD> -0.50 </TD> <TD> 0.50 </TD> </TR>
   </TABLE>
<br><br>Test sensitivity at the optimal cut-point 6 using the Youden-Index J threshold is 0.54 (95% CI 0.33 to 0.74). Test specificity is 0.76 (95% CI 0.7 to 0.81). The likelihood ratio of a positive test is 2.25 (95% CI 1.47 to 3.46). The number needed to diagnose (NND) is 3.33 (95% CI 1.82 to 33.33). Around 34 persons need to be tested to return 10 positive tests.<p><p>When the J index is weighted (Jw) for different considerations the threshold changes. If sensitivity is favoured (75:25 weight on sensitivity compared with specificity), which may be more desirable when the costs of a false positive test are low, for example in a screening scenario which triggers further investigations or brief advice on healthy behaviour, then a lower optimal cut point may be desirable: 2 from the Table. This threshold may be desirable from a clinical or population health perspective. However, individual's may value costs differently, and it is feasible that an individual would take umbrage at being labelled as a heavy or risky drinker if in fact they are not; it also runs the theoretical risk of making false positive individuals less likely to respond to health care advice / interventions. If the costs of a false positive are high, then specificity may be weighted as more favourable (25:75 in the example provided in the Table: producing a cut-point of 10 according to the weighted Jw score.)<br><br>While using a single, simply applied threshold as a cut-point for screening and diagnostic tests may have advantages in practice settings,  there  are  disadvantages  if  a  simple  threshold approach  is  used  in  epidemiological  research  or  policy work where questions focus upon population prevalence and effectiveness of  interventions. Each point, or score, on  a  diagnostic  scale  provides  useful  information  that may be lost if  scores are collapsed together into negative or positive categories. However, studies of such diagnostic tests often simplify their results by calculating sensitivity and specificity and related indicators when compared to a reference standard criterion for the presence or absence of a condition or disease, where all values above a single threshold level are considered 'positive' and all those below it are considered 'negative'. This simplistic approach implies that all test results above the threshold increase the likelihood that the condition or disease is present to exactly the same degree. However, if  the likelihood associated with a range of  different thresholds, for example each point on a diagnostic test  scale,  can  be  calculated  then  much  more  precise estimates  of  the  risk  of  a  condition  or  disease  can  be made. This is particularly important when risk increases proportionately or exponentially with test score. In the Figure below, we show that post-test probability, calculated using Bayes Theorem, varies according to the slope of a curve across the range of test scores.<br><br>![plot of chunk showdetails](figure/showdetails48.png) <center><b>Figure: Plot of observed and estimated probability, by AUDIT-C, of DSM-5 Alcohol Use Disorder: none/mild vs. moderate/severe, for individuals with a positive diagnosis (a), and negative diagnosis (b); and post-test probabilities using Bayes’ Theorem &#40;c)</b></center><br><br><br>



***********


### References


